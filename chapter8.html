<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter8.cr" -->
</head>
<body>

<a name="_0_chapter8diagnostictools"></a><a name="chapter8diagnostictools"></a><h1>Chapter 8: Diagnostic Tools</h1>
<p> In this chapter we shall consider the different kinds of programming errors we all make, before exploring the avenues available to us, as Euphoria coders, to diagnose them, en-route to eliminating them. </p>

<a name="_1_typesoferror"></a><a name="typesoferror"></a><h2>Types of error</h2>
<p> Often because we are not perfect typists we make keying errors. Although we may think of them as merely "typos", the Interpreter has no choice but to treat them as "Syntax Errors". </p>

<p> Likewise, if a variable name is referred to in a place in the code where it isn't meaningful then Euphoria deems that also to be a syntax error. </p>

<p> Code may be syntactically and typographically correct but is flawed when run, either because it doesn't generate the outcome intended or because it fails to complete. This could range from a simple coding error (for example, a loop limit is set to zero or a code block is never executed) to a major failure (like going into an infinite loop or chewing up all the PC's memory). </p>

<a name="_2_theinterpreterprocess"></a><a name="theinterpreterprocess"></a><h2>The Interpreter process</h2>
<p> When an executable Euphoria program is passed to the Interpreter a two-stage process is enacted: </p>
<ol><li>the code in the executable module is checked, both internally and with respect to any "external" library modules <em>include</em>d
</li></ol>
<p>! running </p>
<ol><li>only if no problems occur then the checked code is executed
</li></ol>
<p> Any  errors found are reported by the Euphoria Interpreter. In the case of syntax checking, all such errors are output to a terminal window, which is then left open until the programmer presses the <em>ENTER</em> key. In the case of a "clean" syntax check but run-time errors a file, named, by default, <em>ex/err</em> is generated, which gives a whole range of information about the state of play at the time the critical error occurred. Just the critical error appears on the terminal. </p>

<p> Sometimes the Interpreter carries out both stages successfully, thus yielding a result, but issues one or more warnings, which are deemed to be less-serious errors but which, if unreported, might lead to later problems if the context in which the program is run changes and/or the code is extended. It is always wise to study such warnings and to eliminate their source in many cases. The seriousness can vary markedly, from the case of defining a value (constant or variable) but never actually using it, to instances where deprecated keywords or code blocks are being utilised. (See below for more detail on how to address warnings.) </p>

<a name="_3_diagnosticstrategies"></a><a name="diagnosticstrategies"></a><h2>Diagnostic strategies</h2>
<p> There are three main strands to developing successful software in an efficient manner: </p>
<ul><li>when writing any code block, however small, make sure it can be tested in situ and proven before it is added to a larger code block
</li><li>make sure that the testing process is rigorous and extensive
</li><li>make code blocks as generalised as possible
</li></ul>
<p>! modularity Probably the best way to develop good code is by following the maxim: "small is best". The concept of modularity, whether through separate files, or distinct code blocks within a single file, is the best way of achieving this. Because Euphoria provides the <em>include</em> keyword and it is possible, through other scope keywords, to vary the extent to which a separate module can be accessed, the language is highly suited to this compact style. </p>

<p> Probably the best approach to code-block testing is through the notion of "breakpoints". These are points in a code block where you wish the running process to pause so that some or all of the values referred to in the code can be inspected. Essentially there are two approaches possible: </p>
<ol><li>hand-crafted and essentially bespoke;
</li></ol>
<p>! trace </p>
<ol><li>the <em>trace</em> facility built into the Euphoria Interpreter.
</li></ol>
<p> These are considered in detail below. </p>

<a name="_4_breakpoints"></a><a name="breakpoints"></a><h3>Breakpoints</h3>
<p> A breakpoint can be set in one of two ways: </p>
<ul><li>inserting a piece of pausing code at the desired location
</li><li>if your code editor allows it, setting the breakpoint directly, by clicking some designated area alongside the line of code, typically to the left near to the line number, if visible
</li></ul>
<p> Having set a breakpoint you need to add, immediately before the breakpoint, Euphoria code to interrogate the variables of interest. You can either use the default Euphoria "print", by using the <em>?</em> keyword, or, if more helpful, use either <tt>puts</tt> or <tt>printf</tt> to display the values of interest more elaborately. </p>

<p> There are several ways provided in Euphoria to pause the code but the easiest is to key: </p>

<pre class="examplecode"><font color="#0000FF">getc</font><font color="#880033">(</font><font color="#330033">0</font><font color="#880033">)</font>
</pre>

<p> to create a breakpoint. This can be on a line of its own, or immediately following the inspection line. </p>

<p> Having created the breakpoint, execution will halt there and the inspected values will be visible in the terminal window. Once you have checked them, pressing the <em>ENTER</em> key will re-commence the processing, which will continue to the end of the final code block in the file, or to the next breakpoint, or to the next error, when the processing will stop with a message to the terminal window and an <em>ex/err</em> file dumped in the folder where execution was initiated. (A description of the contents of such a file can be found below.) </p>

<p> I tend to leave the inspection and pausing code even after the code block is proven, but commented out. This is another (subtle) way of using comments to inform the development process.  </p>

<a name="_5_tracing"></a><a name="tracing"></a><h3>Tracing</h3>
<p> The best way of understanding the notion of a trace is to see it as a series of breakpoints. When tracing is switched on (for how see below) every code line thereafter becomes an implicit breakpoint. It is therefore possible to see the effects of stepping through code, line-by-line, observing the program flow and inspecting the values held in each declared variable until either the program ends, an error is encountered or the trace facility is switched off. </p>

<p> Note that tracing (and breakpoint analysis, for that matter) only means something when there is an executable piece of code; that is, trace is a run-time facility. It does, however, step through <em>ALL</em> relevant code, whether in the main program, in an embedded code block, in one of your included module or in any other one - perhaps one of the standard libraries provided in the full distribution (see in later chapters when Core Extensions are introduced). </p>

<p> Tracing is switched on using the special Euphoria switch command: </p>

<pre class="examplecode"><font color="#0000FF">with trace</font>
</pre>

<p> and switched off using: </p>

<pre class="examplecode"><font color="#0000FF">without trace</font>
</pre>

<p> These statements can only appear before or between distinct code blocks and not inside them. </p>

<p> All code between these two statements will be considered. However, only code passed through during execution will actually be analysed. </p>

<p> You only need to use the second statement if you want to cease tracing before the calling program has completed. </p>

<p> In addition to switching on trace, you need to define its "style". </p>

<p> You can determine this via the call: </p>

<pre class="examplecode"><font color="#0000FF">trace</font><font color="#880033">(</font><font color="#330033">n</font><font color="#880033">)</font>
</pre>

<p> where <em>n</em> takes one of three values: 1, 2 or 3. </p>

<p> The value <em>1</em> enables you to see the flow and inspect the values in one screen and still see the normal output in another (so this is the usual value chosen). You can toggle between the two screens using the <em>F1</em> and <em>F2</em> keys. (Note you may need to combine the function keys with <em>ALT</em> and/or <em>CTRL</em> if your PC is set up so that the function keys are set to standard defaults.) The trace screen is, for this option, coloured, which is quite helpful if your terminal screen is the default white-on-black/blue; you can set the trace value to <em>2</em>, however, for a monochrome trace screen. The value <em>3</em> sets up a log file (<em>ctrace.out</em>) where all executed statements are recorded; that is, the precise flow is fully documented. </p>

<a name="_6_thetracescreen"></a><a name="thetracescreen"></a><h4>The trace screen</h4>
<p> The best way to find out about tracing is to use it. I suggest you write a bit of Euphoria code which contains at least one conditional and one loop. Write it such that it takes the form of a <tt>main</tt> procedure with one or more functions called within it. Add several calls to <tt>main</tt>, testing extreme values, normal cases and ones which are out-of-bounds. I give you an example below, based on earlier example code, if you want to use that. Note that the account below relates to this example; one of your own may perform differently. </p>

<pre class="examplecode"><font color="#FF0055">--sum.ex</font>
<font color="#0000FF">function </font><font color="#330033">add_up_integers</font><font color="#880033">(</font><font color="#0000FF">integer </font><font color="#330033">lower, </font><font color="#0000FF">integer </font><font color="#330033">higher</font><font color="#880033">) </font>
<font color="#0000FF">	integer </font><font color="#330033">total = 0 </font>
<font color="#0000FF">	for </font><font color="#330033">n = lower </font><font color="#0000FF">to </font><font color="#330033">higher </font><font color="#0000FF">do </font>
<font color="#330033">		total += n </font><font color="#FF0055">-- same as total = total + n </font>
<font color="#0000FF">	end for </font>
<font color="#0000FF">    return </font><font color="#330033">total </font>
<font color="#0000FF">end function </font>
<font color="#0000FF">procedure </font><font color="#330033">main</font><font color="#880033">(</font><font color="#0000FF">integer </font><font color="#330033">start = 1, </font><font color="#0000FF">integer </font><font color="#330033">stop = 10</font><font color="#880033">) </font>
<font color="#0000FF">    if </font><font color="#330033">stop &gt;= start </font><font color="#0000FF">then </font>
<font color="#0000FF">        printf</font><font color="#880033">(</font><font color="#330033">1, </font><font color="#330033">"Sigma(%d,%d) = %d\n"</font><font color="#330033">, </font>
<font color="#993333">                    {</font><font color="#330033">start, stop, add_up_integers</font><font color="#0000FF">(</font><font color="#330033">start, stop</font><font color="#0000FF">)</font><font color="#993333">}</font><font color="#880033">) </font>
<font color="#0000FF">    else puts</font><font color="#880033">(</font><font color="#330033">1, </font><font color="#330033">"+++ Inappropriate range +++\n"</font><font color="#880033">) </font>
<font color="#0000FF">    end if </font>
<font color="#0000FF">end procedure </font>
<font color="#FF0055">-- execution/testing </font>
<font color="#0000FF">with trace </font>
<font color="#0000FF">trace</font><font color="#880033">(</font><font color="#330033">1</font><font color="#880033">) </font>
<font color="#330033">main</font><font color="#880033">(</font><font color="#330033">0,-3</font><font color="#880033">) </font><font color="#FF0055">-- negative range </font>
<font color="#330033">main</font><font color="#880033">(</font><font color="#330033">,1</font><font color="#880033">) </font><font color="#FF0055">-- start defaulted; no range </font>
<font color="#330033">main</font><font color="#880033">() </font><font color="#FF0055">-- both defaulted </font>
</pre>

<p> Run the code from a terminal window. You will see the trace screen open but only the last bit of code is showing, starting with the call to the <tt>trace</tt> procedure. Step through a line at a time, toggling between the two screens to see the results. Note that the trace screen automatically disappears the moment execution is complete. </p>

<p> Now move the switch statement and <tt>trace</tt> call up above the definition of <tt>main</tt>. You will now see more code in the trace screen. Follow the trace line-by-line, noting the changes: you should see the "jump" to the end of <tt>main</tt> and, at the bottom of the screen, on the "variables bench", the <em>start</em> and <em>stop</em> values. Toggling to the terminal screen will reveal the output, as it unfolds. Watch the trace for the second and third calls. </p>

<p> Finally, move the trace call to the very top of the file. When you begin execution you will see <em>ALL</em> the code in the trace screen. The first call to <tt>main</tt> looks the same as before, but for the other calls you can now see the action inside the <tt>add_up_integers</tt> function. Note that the variables internal to the function now appear on the variables bench, too, but disappear as soon as the function returns control to <tt>main</tt>, as they no longer have any defined status. The third run of <tt>main</tt> enables you to see the embedded loop in action. </p>

<p> (You may notice as you watch the accumulation in the variable <em>total</em> a purple value pop up alongside the main value, from <em>36</em> upwards. Can you understand what the tracing mechanism is doing here?) </p>

<p> This exercise should have convinced you how brilliant the trace mechanism is, being one of the best features of one of the best programming languages. It should also have shown you how, by moving the tracing invocation to various places in the overall code, you can choose which aspects of the code are of interest and which can, in a given case, be discounted. As a final thought, ask yourself the question: "how can I just test the working of the function?" </p>

<p>  As a general rule I advocate the use of <em>trace</em> for small or free-standing code blocks, reserving the breakpoint strategy for testing more complex solutions, with greater numbers of embedded and/or <em>include</em>d code blocks. This is because, as indicated and illustrated above, once the trace is switched on it will go deep into the code, however much it is nested, thus making the process both slow and unnecessary, assuming that you have already proven the "smaller" elements. </p>

<a name="_7_theexerrfile"></a><a name="theexerrfile"></a><h3>The ex.err file</h3>
<p> As indicated above, Euphoria creates this file every time an error arises at run time. The execution process immediately ceases and the Interpreter makes an attempt to report on both the nature and location of the error. Note that this report will always be based on the <tt>first</tt> error encountered, so do not assume that, having corrected the problem, your next call to the Interpreter will automatically result in a success! </p>

<p> The detail can vary enormously, both in meaning and length, but it will always indicate precisely where the critical problem occurred and will, in its own "language", define the type of error causing the failure. You have to get used to this rather terse language, but once mastered, you will begin to appreciate its effectiveness. A good way to learn it is to create quite simple code blocks which you know <em>WILL</em> fail, so that you can match your understanding of "why" with the Interpreter-speak! </p>

<p> The file will also contain a generalised form of a breakpoint analysis, giving the values of all variables at the exact moment of the error. It will also give, where other modules have been <em>include</em>d in the executable main code block, the same detail for all the variables in those modules, also at the point of finding the error. If you are the author of the included modules then this detail may be useful diagnostically, but if someone else wrote them this level of detail may be unhelpful, especially if the Interpreter ran out of steam and failed to include all the detail in YOUR modules. This behaviour is another reason for following the bottom-up (or "inside-out") development strategy advocated above. </p>

<a name="_8_warnings"></a><a name="warnings"></a><h3>Warnings</h3>
<p>  In the same way that <em>trace</em> can be switched on and off, so too can <em>warning</em>s. The code: </p>

<pre class="examplecode"><font color="#0000FF">with </font><font color="#330033">warning</font>
</pre>

<p> switches on warning alerts and </p>

<pre class="examplecode"><font color="#0000FF">without </font><font color="#330033">warning</font>
</pre>

<p> switches them off. </p>

<p> Unlike <em>trace</em>, however, any refinements in setting the types of warnings to be revealed or suppressed, are set on the same line, using Euphoria's set of warning <em>names</em>. </p>

<p> So, for example, the switch: </p>

<pre class="examplecode"><font color="#0000FF">with </font><font color="#330033">warning </font><font color="#880033">{</font><font color="#330033">not_used, no_value</font><font color="#880033">}</font>
</pre>

<p> issues only warnings about variables which either never received (were assigned) a value or was never used in the relevant code block(s). </p>

<p> Euphoria is clever enough to work out when to issue such warnings - it is all to do with the concept of <em>scope</em> which is thoroughly described in Chapter 10. </p>

<p> The warning system is quite elaborate and quite complex, so I don't intend to develop it further here. The Euphoria Language Manual gives extensive detail; what you need to appreciate here is that the system exists, you can use its default settings or change them to your specific needs, but, above all, if you receive warnings then take notice of them, and, as a general rule, eradicate them. (You may, however, in studying other people's code, find instances where the default setting - warnings on - has been deliberately changed; there will be a reason for this, however lost in time it may now be, so leaving well alone is the best remedy!) </p>

<a name="_9_bekindtoothers"></a><a name="bekindtoothers"></a><h3>Be kind to others</h3>
<p> Although not strictly relevant here, I would like to make a plea. Euphoria does such a good job of helping you to create clean and effective code that I ask you to pass on this kindness whenever you write any code which you are making (or intend to make) public. Rather than expecting your (unknown) user to possess the skills and knowledge which you must have acquired to create such an offering, as well as writing appropriate documentation, add any helpers you can to make using your code as easy as it can be. For example, when designing an OOP project (see <a class="external" href="https://sourceforge.net/projects/eucanoop/">the SourceForge repository for detail</a>), I added to the core library a set of routines for inspecting the value(s) of both Objects and Classes. Not only does this help users to learn the concepts, it also adds a diagnostic facility, available at any time, during use of the library.  </p>

<a name="_10_next"></a><a name="next"></a><h2>Next</h2>
<p> We have already considered the built-in data types; next we look at how we can extend the list by <a href="chapter9.html">defining our own</a>. </p>
</body></html>