# Programming in Euphoria #

Welcome to my e-book project - an attempt to write a proper "textbook", with a difference, to introduce the Euphoria programming language.

## What is this repository for? ##

It contains chapters of an e-book, which serves both as an introduction to the language and, hopefully, for those who already know it, also a source of ideas and inspiration.

Readers wanting an introduction to the language should, at least initially, read the book sequentially, whereas the more experienced can choose a topic of interest and read the relevant chapter as a free-standing "essay".

## How is the book organised? ##

The book is divided into two distinct sections.

The first section describes the Core Euphoria Language, as delivered in any version of the Open Euphoria implementation of the language, and embedded within the Interpreter. It consists of 12 chapters designed to be read sequentially and targeted at readers unfamiliar with Euphoria. Each chapter covers a specific aspect of the language which the Core offers.

The second section is designed for someone already familiar with Euphoria's basics but interested in developing a interest beyond that. The first four chapters describe the various ways in which the Euphoria Core can be extended. The remaining chapters take the form of case studies, each illustrating some specific form of extension, exploiting different extension strategies, singly or in combination.

Each chapter in the second section can be read as a stand-alone unit, although each case study makes more sense after reading the chapter outlining the appropriate extension strategy.

Together the contributions in Section 2 demonstrate how extremely flexible and extensible Euphoria is, and, it is hoped, will contribute towards the language receiving better recognition, once its capabilities are better understood.

## How do I get set up? ##

Eventually the book will be available both in html and pdf formats, for the user to select. At present the chapters are only available as html files. So download and select what to read, as appropriate to your needs.

## Contribution guidelines ##

Although initially the book has one author, he would be only too pleased to receive either:

* modifications of, or replacements to, existing chapters
* new chapters, covering additional topics of interest to Euphoria programmers

## Who do I contact? ##

* The Repo owner