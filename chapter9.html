<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter9.cr" -->
</head>
<body>

<a name="_0_chapter9addingyourowndatatypes"></a><a name="chapter9addingyourowndatatypes"></a><h1>Chapter 9: Adding your own data types</h1>
<p> Although Euphoria "comes with" and only ever needs the two data types: <strong>atom</strong> and <strong>sequence</strong>, the Core also recognises a special form of <strong>atom</strong>, known as <strong>integer</strong>, which holds whole number values in the range -1,073,741,824 to +1,073,741,823! </p>

<p> Allowing also the <strong>object</strong> type ("I don't want to decide now" or "it could be either/both") then the Core recognises four types when it comes to declaring variables in Euphoria. </p>

<a name="_1_typesasfunctions"></a><a name="typesasfunctions"></a><h2>Types as functions</h2>
<p> A very important aspect of declaring a variable as <strong>object</strong> is the capacity, incorporated into the Core, to apply tests to determine, at any given time, if a given <strong>object</strong> variable is holding an <strong>atom</strong> or a <strong>sequence</strong>. </p>

<p> A unique feature of Euphoria is that each and every type, whether defined in the Core, or created by a programmer, has an equivalent function, of the same name, attached to it. So, for example, </p>

<pre class="examplecode"><font color="#0000FF">if atom</font><font color="#880033">(</font><font color="#330033">x</font><font color="#880033">) </font><font color="#0000FF">then</font><font color="#330033">...</font>
</pre>

<p> sees the type being used functionally to test for itself. This is a really brilliant feature, as you will soon realise, and it gives the clue how to extend the range of types yourself. </p>

<p> Just as a function is defined by a code block surrounded this way: </p>

<pre class="examplecode"><font color="#0000FF">function </font><font color="#330033">fred</font><font color="#880033">(</font><font color="#330033">...</font><font color="#880033">)</font>
<font color="#330033">... </font>
<font color="#0000FF">end function</font>
</pre>

<p> so a user-defined type is defined in a code block delimited this way: </p>

<pre class="examplecode"><font color="#0000FF">type </font><font color="#330033">fred</font><font color="#880033">(</font><font color="#330033">...</font><font color="#880033">)</font>
<font color="#330033">... </font>
<font color="#0000FF">end type</font>
</pre>

<p> Code blocks for type definitions always return a boolean (TRUE or FALSE) and take, as argument, the "parent" from which the new type is derived. </p>

<p> It seems appropriate to choose, as a first illustration, the concept of a boolean type. </p>

<p> The strategy for defining new types is as follows: </p>
<ul><li>think about the range of values that you want the type to accept
</li><li>think about the need to define any fixed values to enable the definition
</li><li>thus determine what the parent type is (for example, <strong>atom</strong> is the parent of <strong>integer</strong>)
</li><li>write the type function code, ensuring a boolean return value
</li><li>determine whether you want the type to be available only within the defining module, or more widely (see the next chapter for much more on modularity and controlling interfaces)
</li></ul>
<a name="_2_thebooleantype"></a><a name="thebooleantype"></a><h3>The boolean type</h3>
<p> First we need to define the acceptable values: </p>

<pre class="examplecode"><font color="#0000FF">constant </font><font color="#330033">FALSE = </font><font color="#880033">(</font><font color="#330033">1=0</font><font color="#880033">)</font><font color="#330033">, TRUE = </font><font color="#880033">(</font><font color="#330033">1=1</font><font color="#880033">)</font>
</pre>

<p> Clearly this implies that the parent type is <strong>integer</strong>. </p>

<p> Thus we can write the type definition: </p>

<pre class="examplecode"><font color="#0000FF">type </font><font color="#330033">boolean</font><font color="#880033">(</font><font color="#0000FF">integer </font><font color="#330033">this</font><font color="#880033">)</font>
<font color="#0000FF">	return </font><font color="#330033">this = FALSE </font><font color="#0000FF">or </font><font color="#330033">this = TRUE	</font><font color="#FF0055">-- certain to be boolean </font>
<font color="#0000FF">end type</font>
</pre>

<p> We can then use this definition to do things like: </p>

<pre class="examplecode"><font color="#0000FF">if </font><font color="#330033">boolean</font><font color="#880033">(</font><font color="#330033">x</font><font color="#880033">) </font><font color="#0000FF">then </font><font color="#330033">...</font>
</pre>

<p> In a way it is rather surprising that this type isn't already in the Core. (The reason is quite subtle: Euphoria recognises a wider test from its <strong>if</strong> statement: zero [FALSE]; everything else TRUE.) Nevertheless, I am sure that you will want to use the more restricted form, if only to make your coding more readable. </p>

<p> Before moving on take a look at an alternative definition of the <strong>boolean</strong> type: </p>

<pre class="examplecode"><font color="#0000FF">type </font><font color="#330033">boolean</font><font color="#880033">(</font><font color="#0000FF">integer </font><font color="#330033">this</font><font color="#880033">)</font>
<font color="#0000FF">	return find</font><font color="#880033">(</font><font color="#330033">this, </font><font color="#993333">{</font><font color="#330033">FALSE, TRUE</font><font color="#993333">}</font><font color="#880033">) </font><font color="#FF0055">-- uses built-in function find </font>
<font color="#0000FF">end type</font>
</pre>

<a name="_3_othertypesderivedfrominteger"></a><a name="othertypesderivedfrominteger"></a><h2>Other types derived from integer</h2>
<p> The range of possible (sub)types is considerable, but a few suggestions here might trigger useful thoughts. Ideas include positive integers, handles (whether to files or widgets), bytes, printable characters; or more mathematical ideas like prime numbers or squares. </p>

<p> Whatever you choose to define, the checklist above will guide you through the process. (Note that, for something like a character type, the finding-in-a-list method shown in the second definition of boolean is much more likely to be useful than a chain of <em>or</em>s.) </p>

<a name="_4_typesderivedfromsequence"></a><a name="typesderivedfromsequence"></a><h2>Types derived from sequence</h2>
<p> As further illustrations, I am going to give definitions of two very useful (extension) types: the <strong>vector</strong> (a sequence made up entirely of atoms) and the <strong>string</strong> (a sequence made up entirely of characters - we shall have to define what a character is on the way!) </p>

<a name="_5_thevectortype"></a><a name="thevectortype"></a><h3>The vector type</h3>
<p> Here is one way of defining a <strong>vector</strong> type. It assumes that the boolean values have been defined: </p>

<pre class="examplecode"><font color="#0000FF">type </font><font color="#330033">vector</font><font color="#880033">(</font><font color="#0000FF">sequence </font><font color="#330033">this</font><font color="#880033">)</font>
<font color="#0000FF">	for </font><font color="#330033">i = 1 </font><font color="#0000FF">to length</font><font color="#880033">(</font><font color="#330033">this</font><font color="#880033">) </font><font color="#0000FF">do </font>
<font color="#0000FF">		if not atom</font><font color="#880033">(</font><font color="#330033">this</font><font color="#993333">[</font><font color="#330033">i</font><font color="#993333">]</font><font color="#880033">) </font><font color="#0000FF">then </font>
<font color="#0000FF">			return </font><font color="#330033">FALSE </font>
<font color="#0000FF">		end if </font>
<font color="#0000FF">	end for </font>
<font color="#0000FF">	return </font><font color="#330033">TRUE </font>
<font color="#0000FF">end type</font>
</pre>

<p> Study this code and you will learn a lot about type definitions. A typical strategy for sequence-based types is to loop through the elements and "bomb out" if the "inner" condition isn't met. </p>

<a name="_6_thestringtype"></a><a name="thestringtype"></a><h3>The string type</h3>
<p> To give your brain a further challenge here is a very similar definition packaged in a totally different way. (It is offered as an early insight into a very particular approach to programming in general and in working with Euphoria in particular.) </p>

<p> Again the definition assumes the boolean values; also that a <strong>char</strong> type, based on a subset of integers, has been pre-defined. </p>

<pre class="examplecode"><font color="#0000FF">type </font><font color="#330033">string</font><font color="#880033">(</font><font color="#0000FF">sequence </font><font color="#330033">this</font><font color="#880033">)</font>
<font color="#0000FF">	return </font><font color="#330033">isString</font><font color="#880033">(</font><font color="#330033">this</font><font color="#880033">) </font>
<font color="#0000FF">end type </font>
<font color="#0000FF">function </font><font color="#330033">isString</font><font color="#880033">(</font><font color="#0000FF">sequence </font><font color="#330033">this</font><font color="#880033">)	 </font>
<font color="#0000FF">    switch length</font><font color="#880033">(</font><font color="#330033">this</font><font color="#880033">) </font><font color="#0000FF">do </font>
<font color="#0000FF">    case </font><font color="#330033">0 </font><font color="#0000FF">then return </font><font color="#330033">TRUE </font>
<font color="#0000FF">    case else return </font><font color="#330033">char</font><font color="#880033">(</font><font color="#330033">this</font><font color="#993333">[</font><font color="#330033">$</font><font color="#993333">]</font><font color="#880033">) </font><font color="#0000FF">and </font><font color="#330033">isString</font><font color="#880033">(</font><font color="#330033">this</font><font color="#993333">[</font><font color="#330033">1..$-1</font><font color="#993333">]</font><font color="#880033">) </font>
<font color="#0000FF">    end switch </font>
<font color="#0000FF">end function</font>
</pre>

<p> I use this style again in Chapter 17 but there are a few reasons for placing it here. </p>

<p> Although the <em>type</em> statement behaves as if it is a <em>function</em> it really isn't, and sometimes this matters!  </p>

<p> If you want to chain type-definition (for example, string is a specialist form of a byte, which itself is a specialised form of a vector of integers, which is a specialist form of a vector of atoms) then it can be very inefficient to use <em>type</em> to build that hierarchy, because you have loops within loops within loops, etc. In such cases it is better to use a generalised "all-the-same-subtype" <em>function</em> and process the checks once. </p>

<p> Such a generalised function, however, can only do the job if you can call the type-defining function in a special way - known as indirect calling - which is introduced in Chapter 11. (I show this type-defining example there.) </p>

<p> Sometimes you want to leave the choice of which routine to call until the program is actually running. When that choice involves any form of type-related testing then the type-test functionality won't work.  </p>

<p> The choice of a <em>recursive</em> function in the <tt>isString</tt> example not only reminds you that recursion is an alternative strategy available in all versions of Euphoria but it also, if you follow through the logic carefully, points out an apparently different, but totally analogous, way of exiting a code block efficiently whilst carrying the correct value "out" with it! </p>

<a name="_7_chosingastrategy"></a><a name="chosingastrategy"></a><h2>Chosing a strategy</h2>
<p> Whether you choose to define your own types, or stick with only those in the Core, or maybe just with the addition of a few extras, is entirely for you to determine. You can even do it on a case-by-case basis. It is, in part, a matter of style, but, in another respect, it is about your approach to error-trapping. The looser the variable's type definition is the less likely it is to cause a type-check error, so the burden of error checking has to be shifted into your own code and the error-trapping you adopt. The tighter your type definitions are the more you transfer this part of error-checking to the Euphoria Interpreter and away from your internal coding strategy. </p>

<p> How you do this is entirely up to you! </p>

<a name="_8_next"></a><a name="next"></a><h2>Next</h2>
<p> In data (types and values) and code blocks (conditional, looping and procedural) we have the essential tools for constructing working Euphoria programming solutions. So we are now ready to look at another key element of Euphoria programming, which binds these together and aids both their organisation and interchange. It is described fully in <a href="chapter10.html">Modularity</a>. </p>
</body></html>