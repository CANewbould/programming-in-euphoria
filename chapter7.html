<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter7.cr" -->
</head>
<body>

<a name="_0_chapter7programflow"></a><a name="chapter7programflow"></a><h1>Chapter 7: Program flow</h1>
<p> The best way to think about programming is to see it is a collection of coding blocks. </p>

<p> A code block is a set of coherent code which, when executed, runs in its entirity. </p>

<p> Programming consists of creating and organising code blocks and arranging precisely how each block interfaces with other code blocks. </p>

<p> Code blocks can be free-standing, or can be embedded, sometimes quite deeply, inside other code blocks. </p>

<p> What follows is a description of how this concept applies to Euphoria and how code blocks in the language are created, organised and, by convention, made visible. </p>

<p> The most fundamental coding block is a (main) program: a module of code which, when run, yields some form of output. (Strictly speaking, a program might perform some function, or set of functions, entirely in the background, without any visible signs! It is, however, always better, even in such cases, to send some sort of signal to the user, just in case (s)he is about to switch off or carry out some other terminally-critical function!) </p>

<p> In Euphoria such a code block is given a meaningful file name and assigned an extension of either <em>ex</em> or <em>exw</em>. (This distinction is used, not always consistently by Euphoria programmers, to show whether the output is to a "console" [command-prompt or <a href="PowerShell.html">power shell</a> window in MS Windows, or the terminal in Unix-like operating systems] - the <em>ex</em> option - or as some form of graphical output - the <em>exw</em> version - the "w" signifying a form of "window"!) </p>

<p> Unlike some languages, Euphoria has no set layout of a program or indeed the form and order it might take. In examples which are used from now on in this book, a convention has been adopted which is shown below: </p>
<ol><li>Description (as comment) of the program, including name, purpose, version, author and date, with details of recent changes
</li><li>Details of any requirements before the program can be run, including reference to any "external" calls to other modules (see Chapter 10 for details of what this means)
</li><li>A list of any fixed values (<strong>constant</strong>s) required by this program
</li><li>Declaration of any <strong>type</strong>s which the program needs to define (see Chapter 9 for details of how to do this)
</li><li>Declaration of any variable values which this program will use, including, where possible, an initial assignment
</li><li>Definition of any routines which this program will call and which are not defined elsewhere
</li><li>Definition of a <tt>main</tt> routine, usually a <strong>procedure</strong>, which defines how the program can be executed
</li><li>A call (or calls) to <tt>main</tt> with the parameter values needed to generate the desired result
</li></ol>
<p> A further refinement in this layout aids the reading of the code: each and every (sub, or "child") code block is indented to show the level of embedding of that code block. </p>

<p> Within this "parent" code block we already have at least one of these - the <tt>main</tt> routine. This gives us a clue as to how Euphoria sees such code blocks and how they are made visible when reading the code. All "interior" code blocks in Euphoria begin with a keyword and terminate by a repeat of that keyword immediately after the keyword <strong>end</strong>. </p>

<p> Examples of Euphoria code block keywords are: </p>
<ul><li>procedure
</li><li>function
</li><li>if
</li><li>for
</li><li>while
</li></ul>
<p> The first two have already been introduced; the others in this list are the subject of the rest of this chapter. </p>

<a name="_1_conditionalstructures"></a><a name="conditionalstructures"></a><h2>Conditional structures</h2>
<p> Euphoria offers code block structures which allow for different actions (through other - embedded - code blocks) for different circumstances. </p>

<a name="_2_theifendifstructure"></a><a name="theifendifstructure"></a><h3>The if...end if structure</h3>
<p>  This structure defines a code block inside which a question is posed, or a test presented, which results in a binary outcome: the result is either <strong>TRUE</strong> or <strong>FALSE</strong>. (This is ofter known as a "boolean" state.) The code inside the structure lays out the action(s) to be taken as a result of the test being carried out. </p>

<p> Take a very simple case: </p>

<pre class="examplecode"><font color="#0000FF">if </font><font color="#330033">x = TRUE </font><font color="#0000FF">then</font>
<font color="#0000FF">	puts</font><font color="#880033">(</font><font color="#330033">1, </font><font color="#330033">"'x' is TRUE!"</font><font color="#880033">) </font>
<font color="#0000FF">end if</font>
</pre>

<p> which will either send a message to the console/terminal or remain silent. </p>

<p> The <em>if</em> structure has more complex forms. The first has the outline: </p>

<pre class="examplecode"><font color="#0000FF">if </font><font color="#330033">... </font><font color="#0000FF">then</font>
<font color="#FF0055">	-- code block if test TRUE </font>
<font color="#0000FF">else </font>
<font color="#FF0055">	-- code block if test FALSE </font>
<font color="#0000FF">end if</font>
</pre>

<p> Many programmers insist on this extended form of the structure because it forces the writer to consider all possible outcomes of the conditional test. </p>

<p> The most complex form allows alternative conditions to be incorporated into a single code block. Its form is: </p>

<pre class="examplecode"><font color="#0000FF">if </font><font color="#330033">... </font><font color="#0000FF">then</font>
<font color="#FF0055">	-- code block if test TRUE </font>
<font color="#0000FF">elsif </font><font color="#330033">... </font><font color="#0000FF">then </font>
<font color="#FF0055">	-- code block if alternative test TRUE </font>
<font color="#0000FF">else </font>
<font color="#FF0055">	-- code block if initial test FALSE </font>
<font color="#0000FF">end if</font>
</pre>

<p> You can have as many <em>elsif</em>s as you want. </p>

<a name="_3_theswitchendswitchstructure"></a><a name="theswitchendswitchstructure"></a><h3>The switch...end switch structure</h3>
<p>  If you are in danger of building up many <em>elsif</em>s then the <em>switch</em> structure <strong>may</strong> be your salvation. </p>

<p> The condition in the <em>if</em> structure is replaced by the name of a variable, whose value is one of a fixed range of possibilities. Inside the code block you use the keyword <em>case</em> to specify a specific value, or set of values, followed by the code block you wish to execute if that value occurs. This structure also allows for an <em>else</em> option. Here is an example: </p>

<pre class="examplecode"><font color="#0000FF">switch </font><font color="#330033">number </font><font color="#0000FF">then</font>
<font color="#0000FF">	case </font><font color="#330033">1 </font><font color="#0000FF">then </font>
<font color="#0000FF">		puts</font><font color="#880033">(</font><font color="#330033">1, </font><font color="#330033">"one"</font><font color="#880033">) </font>
<font color="#0000FF">	case </font><font color="#330033">2 </font><font color="#0000FF">then </font>
<font color="#0000FF">		puts</font><font color="#880033">(</font><font color="#330033">1, </font><font color="#330033">"two"</font><font color="#880033">) </font>
<font color="#0000FF">	case </font><font color="#330033">3,4,5,6,7,8,9 </font><font color="#0000FF">then </font>
<font color="#0000FF">		puts</font><font color="#880033">(</font><font color="#330033">1, </font><font color="#330033">"3-9"</font><font color="#880033">) </font>
<font color="#0000FF">	case else </font>
<font color="#0000FF">		puts</font><font color="#880033">(</font><font color="#330033">1, </font><font color="#330033">"less than 1 or more than ten"</font><font color="#880033">) </font>
<font color="#0000FF">end switch</font>
</pre>

<p> Again many programmers insist on the <em>else</em> clause, to ensure the integrity of the code block. </p>

<p> If all the conditional code blocks are one-liners, some programmers prefer to lay out conditional structures as illustrated by this example: </p>

<pre class="examplecode"><font color="#0000FF">if </font><font color="#330033">... </font><font color="#0000FF">then </font><font color="#330033">...</font>
<font color="#0000FF">elsif </font><font color="#330033">... </font><font color="#0000FF">then </font><font color="#330033">... </font>
<font color="#0000FF">else </font><font color="#330033">... </font>
<font color="#0000FF">end if</font>
</pre>

<p> or even: </p>

<pre class="examplecode"><font color="#0000FF">if </font><font color="#330033">...</font>
<font color="#0000FF">	then </font><font color="#330033">... </font>
<font color="#0000FF">	else </font><font color="#330033">... </font>
<font color="#0000FF">end if</font>
</pre>

<p> As in all program writing, I recommend choosing a style and then sticking rigidly to it. </p>

<a name="_4_loopingstructures"></a><a name="loopingstructures"></a><h2>Looping structures</h2>
<p> Euphoria also offers code block structures which allow for the same set of actions (through other - embedded - code blocks) to be repeated. </p>

<a name="_5_theforendforstructure"></a><a name="theforendforstructure"></a><h3>The for...end for structure</h3>
<p> For cases where a(n embedded) code block is to be repeated a set number of times the <em>for</em> structure is provided. Its syntax is, in its simplest form: </p>

<pre class="examplecode"><font color="#0000FF">for </font><font color="#330033">counter_variable = LOWER_LIMIT </font><font color="#0000FF">to </font><font color="#330033">UPPER_LIMIT </font><font color="#0000FF">do</font>
<font color="#FF0055">	-- code block to be repeated </font>
<font color="#0000FF">end for</font>
</pre>

<p> The <em>counter_variable</em> can be any name you fancy (<em>i</em>, <em>n</em> and <em>index</em> are popular choices) but make sure that the name hasn't been used elsewhere outside the <em>for</em> structure (try to work out why this could cause problems before leaving this sub-section); the LOWER_LIMIT is typically equal to 1 and UPPER_LIMIT equal to the number of repeats desired. </p>

<p> The complete definition of the structure adds an extra element, which in the simpler case defaults (to 1): </p>

<pre class="examplecode"><font color="#0000FF">for </font><font color="#330033">counter_variable = LOWER_LIMIT </font><font color="#0000FF">by </font><font color="#330033">INCREMENT </font><font color="#0000FF">to </font><font color="#330033">UPPER_LIMIT </font><font color="#0000FF">do</font>
<font color="#FF0055">	-- code block to be repeated </font>
<font color="#0000FF">end for</font>
</pre>

<p> so, for example, INCREMENT = -1 allows downward counting, whilst INCREMENT = 2 applies only to even-numbered values of the index. </p>

<p> Often the counter_variable is referred to inside the code block being repeated, as the example below illustrates. It shows how you can utilise a <em>for</em> loop to add up integers. </p>

<pre class="examplecode"><font color="#0000FF">integer </font><font color="#330033">total = 0</font>
<font color="#0000FF">for </font><font color="#330033">n = 1 </font><font color="#0000FF">to </font><font color="#330033">10 </font><font color="#0000FF">do </font>
<font color="#330033">	total += n </font><font color="#FF0055">-- same as total = total + n </font>
<font color="#0000FF">end for</font>
</pre>

<p> At the termination of the loop the <em>total</em> variable holds the sum of the first ten integers. </p>

<p> What, you might wonder, is the value of <em>n</em> at this same point? The answer is that it is undefined - the variable only exists for the life of the <em>for</em> loop! </p>

<p> In the great majority of circumstances the limits and the increment are fixed (cf. <strong>constant</strong>s) but you can use variable values instead. For example, we can generalise the above code: </p>

<pre class="examplecode"><font color="#0000FF">integer </font><font color="#330033">total = 0, highest</font>
<font color="#FF0055">-- set highest before entering this loop </font>
<font color="#0000FF">for </font><font color="#330033">n = 1 </font><font color="#0000FF">to </font><font color="#330033">highest </font><font color="#0000FF">do </font>
<font color="#330033">	total += n </font><font color="#FF0055">-- same as total = total + n </font>
<font color="#0000FF">end for</font>
</pre>

<p> or, going one step further </p>

<pre class="examplecode"><font color="#0000FF">function </font><font color="#330033">add_up_integers</font><font color="#880033">(</font><font color="#0000FF">integer </font><font color="#330033">lower, </font><font color="#0000FF">integer </font><font color="#330033">higher</font><font color="#880033">)</font>
<font color="#0000FF">	integer </font><font color="#330033">total = 0 </font>
<font color="#0000FF">	for </font><font color="#330033">n = lower </font><font color="#0000FF">to </font><font color="#330033">higher </font><font color="#0000FF">do </font>
<font color="#330033">		total += n </font><font color="#FF0055">-- same as total = total + n </font>
<font color="#0000FF">	end for </font>
<font color="#0000FF">	return </font><font color="#330033">total </font>
<font color="#0000FF">end function</font>
</pre>

<p> to construct a functional code block for all such circumstances. </p>

<a name="_6_thewhileendwhilestructure"></a><a name="thewhileendwhilestructure"></a><h3>The while...end while structure</h3>
<p> Sometimes the programmer doesn't know in advance how many times to repeat the key code block, because the repetition depends on conditions which are set elswhere in the code, that is, before the repetition commences. In such cases Euphoria provides a <em>while</em> code block structure. It takes the form of: </p>

<pre class="examplecode"><font color="#0000FF">while </font><font color="#330033">condition </font><font color="#0000FF">do</font>
<font color="#FF0055">	-- code block to be repeated </font>
<font color="#0000FF">end while </font>
</pre>

<p> Here <em>condition</em> can be a fixed value (<strong>constant</strong>), a variable value which yields a true/false result, or an expression (containing any number of variable and/or fixed values) which likewise yields a true/false result. </p>

<p> On execution the condition is evaluated and, if true, the embedded code block is executed. So, to effect any repetition, the embedded code block has to change the condition each time.So, if it helps to understand, you can think of the <em>while</em> as a form of repeated <em>if</em>s. </p>

<p> The "adding-up" example above would look like this using a <em>while</em> loop: </p>

<pre class="examplecode"><font color="#0000FF">integer </font><font color="#330033">total = 0, n = 1</font>
<font color="#0000FF">while </font><font color="#330033">n &lt;= 10 </font><font color="#0000FF">do </font>
<font color="#330033">	total += n </font><font color="#FF0055">-- same as total = total + n </font>
<font color="#330033">	n += 1 </font><font color="#FF0055">-- same as n = n + 1 </font>
<font color="#0000FF">end while</font>
</pre>

<p> So the incrementing variable is indeed changed within the embedded code loop. (Note also that <em>n</em> is still defined, and is equal to <em>11</em>, after the <em>while</em> code block is exited.) </p>

<p> The functional definition equivalent would look like this: </p>

<pre class="examplecode"><font color="#0000FF">function </font><font color="#330033">add_up_integers</font><font color="#880033">(</font><font color="#0000FF">integer </font><font color="#330033">lower, </font><font color="#0000FF">integer </font><font color="#330033">higher</font><font color="#880033">)</font>
<font color="#0000FF">	integer </font><font color="#330033">total = 0 </font>
<font color="#0000FF">	while </font><font color="#330033">lower &lt;= higher </font><font color="#0000FF">do </font>
<font color="#330033">		total += lower </font><font color="#FF0055">-- same as total = total + lower </font>
<font color="#330033">		lower += 1 </font><font color="#FF0055">-- same as lower = lower + 1 </font>
<font color="#0000FF">	end while </font>
<font color="#0000FF">	return </font><font color="#330033">total </font>
<font color="#0000FF">end function</font>
</pre>

<p> which, as well as working fine, shows us in a different way something quite profound about both the Euphoria language and the inner workings of loops. The variable <em>lower</em> here has two distinct roles: </p>
<ol><li>it is a proxy for the value passed whenever the function is called
</li><li>it doubles up as a localised identity - a counting variable solely within the loop
</li></ol>
<p> Please don't worry unduly about these points if you are struggling with this duality, as the following is just as good: </p>

<pre class="examplecode"><font color="#0000FF">function </font><font color="#330033">add_up_integers</font><font color="#880033">(</font><font color="#0000FF">integer </font><font color="#330033">lower, </font><font color="#0000FF">integer </font><font color="#330033">higher</font><font color="#880033">)</font>
<font color="#0000FF">	integer </font><font color="#330033">total = 0, n = lower </font>
<font color="#0000FF">	while </font><font color="#330033">n &lt;= higher </font><font color="#0000FF">do </font>
<font color="#330033">		total += n </font><font color="#FF0055">-- same as total = total + n </font>
<font color="#330033">		n += 1 </font><font color="#FF0055">-- same as n = n + 1 </font>
<font color="#0000FF">	end while </font>
<font color="#0000FF">end function</font>
</pre>

<a name="_7_jumpingoutofloops"></a><a name="jumpingoutofloops"></a><h3>Jumping out of loops</h3>
<p> If you are being very observant you might wonder why a fixed-value condition has any merit in programming. A <strong>constant</strong> never changes so what purpose could a fixed condition contribute towards repetition? </p>

<p> Consider the case where the value of <em>condition</em> is <em>TRUE</em>, where <em>TRUE</em> is defined in an obvious manner. So we are looking at: </p>

<pre class="examplecode"><font color="#0000FF">while </font><font color="#330033">TRUE </font><font color="#0000FF">do</font>
<font color="#FF0055">	-- code block to be repeated </font>
<font color="#0000FF">end while</font>
</pre>

<p> This is called an "infinite loop" because it would appear to go on for ever! </p>

<p> How and why could such a structure have any value? Consider the case of reading the contents of a file, character-by-character and storing the accumulated values in a <strong>sequence</strong>. Euphoria issues a value of <em>-1</em> when the end of file marker is detected, so consider this bit of (incomplete) code: </p>

<pre class="examplecode"><font color="#0000FF">integer </font><font color="#330033">fn = </font><font color="#0000FF">open</font><font color="#880033">(</font><font color="#330033">"..."</font><font color="#330033">, </font><font color="#330033">"r"</font><font color="#880033">) </font><font color="#FF0055">-- file name required here</font>
<font color="#0000FF">sequence </font><font color="#330033">letters = </font><font color="#880033">{} </font>
<font color="#0000FF">while </font><font color="#330033">TRUE </font><font color="#0000FF">do </font>
<font color="#0000FF">	integer </font><font color="#330033">char = </font><font color="#0000FF">getc</font><font color="#880033">(</font><font color="#330033">fn</font><font color="#880033">) </font>
<font color="#0000FF">	if </font><font color="#330033">char = -1 </font><font color="#0000FF">then exit </font>
<font color="#0000FF">	else </font><font color="#330033">letters &amp;= char </font>
<font color="#0000FF">	end if </font>
<font color="#0000FF">end while</font>
</pre>

<p> Here the keyword <em>exit</em>, activiated when the end-of-file code is passed, forces Euphoria to leave the loop, with immediate effect. </p>

<p> Note that, after this apparently abrupt action, <em>letters</em> contains the accumulated set of characters from the file; but the variable <em>char</em> is now undefined. </p>

<p> I think you will agree that the above code is rather a neat way of doing the job and is an improvement on a more conventional use of <em>while</em> to do the same task. Why not have a go at writing that alternative and analysing why the infinite loop approach is better? </p>

<p> Before leaving this topic I should admit that there are more extensions to a <em>while</em> code block structure that are useful on occasions, of which the file-reading example is one. Also there is a sister structure, <em>loop</em>, which ensures that the embedded code block is always executed at least once. In short, whilst <em>while</em> tests before action; <em>loop</em> acts before testing. </p>

<a name="_8_next"></a><a name="next"></a><h2>Next</h2>
<p> We now have an understanding of both fixed code blocks (routines) and flexible ones (structures). So we are now equipped to try to write programs using Euphoria. Writing programs, unless you are unusually brilliant, involves a helter-skelter ride between periods of success and downright useless dejection! To avoid these latter parts spoiling your life, or putting you off programming altogether, we really do need to take a look at the way Euphoria deals with errors and what tools it offers to help identify the causes, so next we look at <a href="chapter8.html">how to handle errors</a>. </p>
</body></html>