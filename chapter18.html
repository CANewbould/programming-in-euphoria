<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter18.cr" -->
</head>
<body>

<a name="_0_euphoriasstandardlibraries"></a><a name="euphoriasstandardlibraries"></a><h1>Euphoria's Standard Libraries</h1>
<p> Whenever anyone downloads a Euphoria distribution the Interpreter comes with lots of additional material. In particular, there is, embedded within the <em>include</em> folder a <em>std</em> folder holding the full set of Standard Libraries appropriate to that distribution. (For MS Windows, look inside the main folder in which Euphoria is stored; for Unix-based distributions, the sub-folder is normally found in <em>/usr/local/include</em>, unless you have carried out a non-standard installation.)</p>

<p> These library modules, as indicated in earlier chapters, are thematically based, and expand on the original set of 13 modules, which you can still find at the higher level in the folder hierarchy. In the rest of this chapter I shall select from among the most frequently used library modules and outline the range of features to be found within. It should be noted that the documentation in the "manual" for each standard library module not only describes the visible elements of that module but also adds, under the same thematic heading, details of the built-in elements which the developers think connect to that theme. (Note that there are specialist chapters covering a couple of the standard libraries, so those details are not replicated here.)</p>

<a name="_1_ioe"></a><a name="ioe"></a><h2>io.e</h2>
<p> Probably the most frequently utilised module is <em>io.e</em>, which augments the built-in input/output routines. Added to the two routines <tt>getc</tt> and <tt>gets</tt> are a range of low-level file-reading routines for inputting bytes, in groups, in different numerical compositions or as (user-specified) delimited strings. There are corresponding routines which enable byte-related data to be output to a file. You can also write formatted textual material to a file.</p>

<p> Methods for moving and locating a "pointer" around inside a file's contents are offered in this library, as are methods for locking and unlocking access to a file.</p>

<p> Whilst all these methods could be described as "low-level access", the library also provides a number of routines which operate as "high-level". You can read all the contents of a file at one go, read or process the (textual) contents line-by-line, with parallel routines for writing or appending to files. A feature of these high-level routines is that they are capable of operating at the low level of file-handles or by directly specifying the file's name. Selecting which to use will depend on whether or not the file is already opened, or needs to be left open for further access.</p>

<a name="_2_filesyse"></a><a name="filesyse"></a><h2>filesys.e</h2>
<p> Whilst <em>io.e</em> operates on the contents of files, the <em>filesys.e</em> library operates on the file system: drives, folders and files.</p>

<p> You can analyse any folder structure as well as examine the size and contents of a particular folder, including details of creation and current status of each file within. You can create new folders or delete/destroy existing ones, with some flexibility about sub-folder retention. Euphoria has a concept of "the current directory", which you return as a text string. You can reset it within a code block, but the value of the original is always recoverable.</p>

<p> You can carry out a lot of file management functions from within a code module. You can test for a file's existence, overall or within a range of folders; you can find its length, create a checksum value for it and interrogate other features; you can change its name, move it, copy it or delete it. There is even a routine for handling temporary files so that they are uniquely identified.</p>

<p> This library also offers a range of facilities for parsing file names, including extracting the different parts and determining whether a path specification is absolute or relative. It also allows for the reverse process: building up a file name from constituent parts.</p>

<p> As well as all this file and folder management, you can also get detailed feedback about drives, both fixed and removable.</p>

<a name="_3_consolee"></a><a name="consolee"></a><h2>console.e</h2>
<p> This library extends the facilities for terminal-based user interaction. It adds various ways of holding the terminal window open at the end of a main code block and adds a couple of very useful functions for user-interaction, using a prompt-and-reply strategy.</p>

<p> There are routines for checking whether the current application has a terminal window, for controlling the use of the standard break mechanism (Ctrl-C) and there is a range of routines for directly programming areas of the screen and even individual characters therein. You can also specify the dimensions of your terminal screen and charge the cursor style within it. </p>

<a name="_4_searche"></a><a name="searche"></a><h2>search.e</h2>
<p> This library has a range of routines which, as the manual entry indicates, can be divided into three groups:</p>
<ul><li>equality
</li><li>finding
</li><li>matching
</li></ul>
<p>In fact both equality-testing routines are built into the Core, so really there are only two.</p>

<p> The process of finding is a sequence-based process, looking for objects in a sequence. It returns pointers (integers &gt;= 1) to where, in the main sequence a specified object appears, using <em>0</em> as a proxy for <em>FALSE</em> - not found.</p>

<p> In Open Euphoria the function <tt>find</tt> now covers a lot of one's routine searches, because you can determine exactly what sub-sequence your search is limited to. More refined searching can find multiple instances (as elements in a returned sequence) and/or search for multiple values in one pass, or search within sub-sequences, essentially to any level of nesting. You can even start your search at the end of a sequence and do a reverse search, effectively doing <tt>find(reverse</tt>(seq), ...) in a single step.</p>

<p> There are also "conversion" routines, which offer "search-and-replace" facilities.</p>

<p> Matching is really searching where the items to be found are sequences, and, indeed often, text strings. Most of the extended "..find.." routines have a "..match.." equivalent. In addition there is a range of functions which return <em>TRUE/FALSE</em> values for testing if a sequence begins with, ends with, or contains a specific value. You can also pick a value in one sequence list, depending upon the corresponding value in a parallel list, and even extend this idea to a "grid" - a two-dimensional sequence.</p>

<a name="_5_sequencee"></a><a name="sequencee"></a><h2>sequence.e</h2>
<p> As the <strong>sequence</strong> type is so central to Euphoria programming, it is no surprise that one of the most extensive standard libraries is devoted to sequence manipulation. Below is a description of most, but not all, the routines offered.</p>

<p> The library can be divided into a number of different operational categories: manipulation; adding; extracting, removing, replacing elements of a sequence; changing the shape of a sequence.</p>

<p> You can check whether an index you want to apply to a sequence is valid. You can retrieve elements or store new ones in a sequence, regardless of how deeply nested the elements are. You can reverse all elements of a sequence or rotate all or part of it. You can turn a set of vectors into a matrix by columnisation. You can apply a function to every element of a sequence, via its routine-id, and even, apply more than one routine sucessively in a single call. You can selectively change elements by having from/to sets of values.</p>

<p> As well as the built-in routines of <tt>append</tt>, <tt>prepend</tt>, <tt>insert</tt> and <tt>splice</tt>, there are routines for padding out a sequence (at the front or the back) and for selectively adding or removing specific items to/from a sequence.</p>

<p> Whilst <tt>head</tt> and <tt>tail</tt> are built into the Core, other interrogation routines can take out or replace a slice from a sequence (including a vertical slice of a matrix-style sequence).</p>

<p> You can select, or remove, all instances of particular elements in a sequence, or just remove duplicates. You can construct a numeric sequence of index numbers which can be used to extract elements from another sequence. You can "filter" a sequence, in a manner exactly analogous to "list comprehensions" as they are known in other languages; again the routine-id is utilised to achieve this. (Some functional equivalents - for example, "in" and "ge" - are built into this procedure.)</p>

<p> You can split a sequence into sub-sequences using a delimiter; this is very useful for text source parsing. You can also use a set of delimiters to split still further. You can do exactly the reverse of splitting, by joining a set of sequences to yields a single sequence with the sources connected by a common delimiter. You can even combine a set of sequences into a single sequence and have the resulting elements sorted! You can split a sequence into equal-length chunks; the trailing chunk may be smaller. You can flatten a nested sequence, so that all elements are at the first level of nesting. You can divide a sequence into three parts, depending on whether an element is greater than, equal to, or less than a given value; the result could give one or two empty sequences. You can even compare two sequence, not just for equality but also to obtain an index of similarity!</p>

<p> What may be surprising is that also in this library are some routines for sequence creation: both for repetition of a (sub)-sequence and for creating sequences made up of a numerical series. </p>

<a name="_6_texte"></a><a name="texte"></a><h2>text.e</h2>
<p> We now move from sequences in general to text-based sequences in particular. These are the things which other languages might define as "strings". In Open Euphoria, however, there is a tendency to work from the most abstract of types (the <strong>object</strong>) most of the time, so this library's routines treat an <strong>atom</strong> as a "character" and a *sequence* as being, potentially, more complex than a vector of characters.</p>

<p> At the most basic level, you can convert any object value into a string, using <tt>sprint</tt>. You can trim a sequence at the beginning, the end or both.</p>

<p> You can qualify the range of characters on which the functions <tt>upper</tt> and <tt>lower</tt> apply before applying them. As implied, they convert to upper- and lower-case respectively. There is also a function <tt>proper</tt> which converts a string sequence into title case.</p>

<p> You can embed a sequence in quotation marks, even if the input already has them; you can also reverse the process. You can manage extra escape characters in strings.</p>

<p> The <tt>format</tt> function offers a general-purpose tool for generating formatted output, with values being substituted into respective format specifiers.</p>

<p> Routines also exist for handling key/value pairs.</p>

<p> Finally, there is a function that allows you to wrap a textual source, with options governing width, end-of-line delimiter and between-word delimiter.</p>

<a name="_7_errore"></a><a name="errore"></a><h2>error.e</h2>
<p> We have already considered, in Chapter 8, various aspects of error management and diagnostics. This standard library adds further features which you may wish to utilise in your handling of warnings and errors.</p>

<p> In the event of a run-time failure (known as a "crash" in Euphoria, as in many other languages) you can determine what message you wish to sent to the terminal. You can locate different messages at different points in your code to add a further layer of diagnosis. You can even insert a deliberate crash into your code if certain critical conditions are not met, or there are locations within the code that shouldn't be visited in a successful execution; either through the built-in <tt>abort</tt> routine, or with a formatted message using <tt>crash</tt>. In addition you can write your own piece of code to be executed on encountering a crash. The routine's "id" is used to interact with Euphoria's error-handling mechanism.</p>

<p> As already indicated, the crash file report goes to a file called <em>ex.err</em>, located in the folder in which the execution is initiated. You can change the name, and even location, of this file.</p>

<p> Warnings, as indicated earlier, automatically get sent to the Standard Error Stream, but they can be redirected within your code to a file of your choice.</p>

<p> You can issue your own warnings in addition to those generated by the Interpreter - a point I forgot to mention in Chapter 8. (An obvious example, is to insert a warning where some feature is being deprecated, with the preferred alternative referenced in the warning message.) All warnings (system and coded) will be displayed whenever the <em>warning</em> switch is on.</p>

<a name="_8_inconclusion"></a><a name="inconclusion"></a><h2>In Conclusion</h2>
<p> There are lots more module libraries in the standard library folder in addition to the few chosen for detailed coverage here. The <a class="external" href="https://openeuphoria.org/docs/apiref.html#_781_apireference">manual</a> has a section, entitled "API Reference" where details of all the libraries are provided. The individual documentary elements are usually quite thorough, and all have illustrative examples, so you shouldn't normally have any need to delve into the code to find out how a library routine works. (Unfortunately in a few cases there is no generalised detail about the library theme, so you may need to delve deeper occasionally to determine if a given library will help you in the particular task you are involved in.) </p>
</body></html>