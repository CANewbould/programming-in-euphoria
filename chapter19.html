<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter19.cr" -->
</head>
<body>

<a name="_0_chapter19interfacingwithclanguagelibraries"></a><a name="chapter19interfacingwithclanguagelibraries"></a><h1>Chapter 19: interfacing with C language libraries</h1>
<p> Many extant libraries, on all the major Operating System (OS) platforms, were originally written in the C programming language. Whilst some of these now have changed, and many others use alternative language bases, there is still a wide range of productive libraries available, written in C. </p>

<p> Whilst Core Euphoria doesn't directly offer a means to interface with C, it offers the capacity to "convert" Euphoria code into C and hence compile programs into very efficient executables (see Chapter 12 for the full set of approaches). But then, of course, you are working in a C environment and not a Euphorian one! </p>

<p> It is easy to build a small extension module, however, only using Euphoria Core's machine-code interface and built-in routines, which offers sufficient library functionality to enable a Euphoria programmer to access the C functions in a "dll" (Windows) or "so" (*nix) and build Euphoria code accordingly. </p>

<p> In this chapter I will outline the contents of this module and then use it to build a very simple case study: a message-box facility, using the IUP GUI toolkit, which runs on both MS Windows and Linux systems.     The module, called <em>c.e</em>, contains only the core functionality needed to access C-language libraries and exploit the functionality stored therein. It has been constructed solely for the purpose of writing this chapter, but it is, of course, fully functional for any other use. The routines and values found in the module can all be found, invariably with other names or differently cast in modules stored in the "standard" libraries found in the Open Euphoria distributions. What follows is a general description of the contents of this module; in the next section the full detail is given, followed by the illustration of its use, which suggests also the format of application modules calling this library. </p>

<p>  The module designed to access C-language modules includes declarations, as constants, of the types which are used in the C language. The <em>C_INT</em> and <em>C_POINTER</em> types are most frequently used, the latter often as a pointer to a C-language textual string: C stores its version of a string in a memory address terminated by a zero and often called a "null-terminated string." In order to pass a string as a parameter of a routine in Euphoria we need to use an appropriate "translation" function in order to send a memory address of the kind a C-language routine understands. </p>

<p> Some C-language routines return a null-terminated string (as a <em>C_POINTER</em>). In order to access the string located at that address we need a reverse-translator function. </p>

<p> Another distinguishing feature of the C language not found in Euphoria, is the ability to change a parameter's value inside the function. (In Euphoria we pass "by value" and not "by reference".) Essentially all C-language functions can only return ONE value at most (there is no simple equivalent to Euphoria's <strong>sequence</strong>), whether it be a numerical value or a pointer, which could be to a complex "array". So the only way to "return" more than one value is to modify the parameter value(s) as well. </p>

<p> So, on occasion we need to build this into our coding. This is best achieved by incorporating everything into a simple function, which typically builds the sequence of parameters, refers to the C-function and returns a <strong>sequence</strong> value containing not only the C-function's return value but also, as in the case described here, any additional parameter values which the function has modified. </p>

<p> Even in perfectly simple cases of calling C routines, programmers often use this approach as it provides a uniform Euphoria-only experience regardless of the variations found in the shared library. </p>

<p>  The process of linking Euphoria to C is effected by the use of handles. The process of "opening" a shared library returns a "large" number handle, which can be used at any time to link to the library's contents. </p>

<p> Likewise, every function declared inside the shared library, when defined in Euphoria, returns a handle. These handles are small integers, starting at <em>0</em> and incrementing as other C-functions are declared to Euphoria. As in the library case, these handles can be used to identify the specific function when called in Euphoria code. Obviously to declare a function properly you need to access some documentation which describes not only the function's result but also the number and character of all its arguments. </p>

<p> The module contains routines for both <em>defining</em> and <em>calling</em> (one for functions and one for procedures) C-language functions. When a C-function is defined within Euphoria a handle is returned and assigned to an identifier; this identifier is then used to call the C-function indirectly inside Euphoria. </p>

<p> As hinted at above, the ability to do this work is built into Euphoria's Core, through both core routines and the Core's provision for incorporating machine-code routine calls directly into Euphoria code. Indeed not only is the provision built-in, so too are the machine-code facilities, via what Euphoria calls "M-codes" - the machine-code execution address. </p>

<a name="_1_thecelibrary"></a><a name="thecelibrary"></a><h2>The c.e library</h2>
<p> Now for the detail of this interface library. </p>

<a name="_2_scope"></a><a name="scope"></a><h3>Scope</h3>
<p> Firstly, the visible routines and values are all declared to be <em>export</em> as the recommended programming strategy is to write a shared library-specific module "to sit on top of" <em>c.e</em> and to <em>include</em> this directly, and not <em>c.e</em>, when writing application code. </p>

<a name="_3_openingasharedlibrary"></a><a name="openingasharedlibrary"></a><h3>Opening a shared library</h3>
<p> A shared library is opened by calling the <em>Clib</em> function. It is a "creator" function, akin to a "Constructor" in OOP. It returns the handle which identifies the shared library to Euphoria. </p>

<p> Its signature is: </p>

<pre class="examplecode"><font color="#0000FF">export function </font><font color="#330033">Clib</font><font color="#880033">(</font><font color="#0000FF">sequence </font><font color="#330033">path_name</font><font color="#880033">)</font>
</pre>

<p> where <em>path_name</em> is, at minimum, the operating system's name for the shared library. Further text may be needed if the shared libraries are not in one of the "the obvious places" for the storage of "executables". The definition of <tt>Clib</tt> contains the first of our machine-code "M code" calls. </p>

<a name="_4_definingcfunctionsineuphoria"></a><a name="definingcfunctionsineuphoria"></a><h3>Defining C-functions in Euphoria</h3>
<p> To do this we call the function <tt>define</tt>, which has quite a complex set of arguments. Its signature is: </p>

<pre class="examplecode"><font color="#0000FF">export function </font><font color="#330033">define</font><font color="#880033">(</font><font color="#0000FF">atom </font><font color="#330033">clib, </font><font color="#0000FF">sequence </font><font color="#330033">routine_name, </font><font color="#0000FF">sequence </font><font color="#330033">taking=</font><font color="#993333">{}</font><font color="#330033">, </font><font color="#0000FF">atom </font><font color="#330033">returning=0</font><font color="#880033">)</font>
</pre>

<p> where </p>
<ol><li><em>clib</em> is the handle returned by <tt>Clib</tt> that points to the shared library of interest
</li><li><em>routine_name</em> is the exact name of the C-function, as specified in the shared library, cast as a string of text, and prefaced by '+' if the shared library uses the <em>cdecl</em> coding convention
</li><li><em>taking</em> is a vector-like <strong>sequence</strong> of argument types, expressed as C-language codes (C_INT, C_POINTER, etc)
</li><li><em>returning</em> is the C-type of the return value of the C-function, with the default of <em>0</em> signifying a <em>void</em> C-language function
</li></ol>
<p> As with <em>Clib</em>, the return value of a call to <tt>define</tt> is a handle (starting at <em>0</em> and incrementing each time a new link is defined), which acts as the direct link, in Euphoria code, to the C-function in the shared library. </p>

<p> This is the second of our machine-code "M code" calls.  To invoke the C-function we use one or other of the Euphoria Core routines <tt>c_func</tt> or <tt>c_proc</tt>, which are used by Euphoria to call non-void and void C-functions, respectively. The signatures are aligned: </p>

<pre class="examplecode"><font color="#0000FF">global procedure c_proc</font><font color="#880033">(</font><font color="#0000FF">integer </font><font color="#330033">void_handle, </font><font color="#0000FF">sequence </font><font color="#330033">params=</font><font color="#993333">{}</font><font color="#880033">)</font>
</pre>

<p> and </p>

<pre class="examplecode"><font color="#0000FF">global function c_func</font><font color="#880033">(</font><font color="#0000FF">integer </font><font color="#330033">handle, </font><font color="#0000FF">sequence </font><font color="#330033">params=</font><font color="#993333">{}</font><font color="#880033">)</font>
</pre>

<p> where </p>

<p> <em>params</em> matches the sequence used to <tt>define</tt> the handle, but using the corresponding Euphoria types as values. </p>

<p> It is common practice to "wrap" definition and call in a short Euphoria routine, often given the same name as the C-function. In some cases the definition is made outside such a routine to avoid excessive repetition if the particular function is likely to be frequently required by applications. On the other hand, you can find wrapper libraries where all the defining is done outside the routine-definitions, which slows down processing of short programs and those calling its C-functions once only. </p>

<p> Note that, if you want to make the closest match of wrapper to shared library then you could make all your wrapper routines into functions and call them all using <tt>c_func</tt> but ignoring the <em>void</em> return value. </p>

<p> The rest of the <em>c.e</em> library consists of the declaration of the various C-types. </p>

<a name="_5_thestartofaniuplibrary"></a><a name="thestartofaniuplibrary"></a><h2>The start of an IUP library</h2>
<p>  IUP has been chosen for this illustration as it is available for more than one operating system. The IUP distributions call the MS Windows shared library <em>iup.dll</em>; in Linux the library is <em>libiup.so</em>. </p>

<p> Because IUP is available in both 32-bit and 64-bit versions the user needs some flexibility, either with regard to re-naming or location of the desired library, so the library module <em>iup.ew</em> includes an initialisation routine, <tt>Iup</tt>, which takes a single argument: the path-name giving the location of the IUP shared library the user requires. It passes this path-name, in full, directly to <tt>Clib</tt> in <em>c.e</em>. </p>

<p> The procedure <tt>Iup</tt> either results in a successful call to <tt>Clib</tt>, which it does silently, or issues a message, to the standard error device, declaring that the library could not be found and, after a user response, will abort. </p>

<p> Consequently all calls to activate the <em>iup.ew</em> library should begin: </p>

<pre class="examplecode"><font color="#0000FF">include </font><font color="#330033">iup.ew</font>
<font color="#330033">Iup</font><font color="#880033">(</font><font color="#330033">...</font><font color="#880033">)</font>
</pre>

<p> In the illustration given here the <em>iup.ew</em> library contains definitions of just four C-function links: </p>
<ol><li><tt>IupOpen</tt>: to open an IUP session
</li><li><tt>IupClose</tt>: to close the opened session
</li><li><tt>IupMessage</tt>: to display a message-box
</li><li><tt>IupVersion</tt>: to establish the version of the IUP library being called
</li></ol>
<p> Of these only <tt>IupMessage</tt> takes any parameters. These are: </p>
<ol><li><strong>sequence</strong> <em>message</em>: the text message to be placed inside the box
</li><li><strong>sequence</strong> <em>caption</em>: the text message-box's caption (title)
</li></ol>
<a name="_6_asimpleexample"></a><a name="asimpleexample"></a><h2>A simple example</h2>
<p> The file <em>iup.exw</em> contains the following code: </p>

<pre class="examplecode"><font color="#FF0055">--iup.exw</font>
 
<font color="#0000FF">include </font><font color="#330033">iup.ew </font>
 
<font color="#330033">Iup</font><font color="#880033">(</font><font color="#330033">"iup.dll"</font><font color="#880033">) </font>
<font color="#330033">IupOpen</font><font color="#880033">() </font>
<font color="#330033">IupMessage</font><font color="#880033">(</font><font color="#330033">"This is the message"</font><font color="#330033">, </font><font color="#330033">"IUP Version" </font><font color="#330033">&amp; IupVersion</font><font color="#993333">()</font><font color="#880033">) </font>
<font color="#330033">IupClose</font><font color="#880033">()</font>
</pre>

<p> and, when run, displays a message-box centrally on the screen, showing the IUP version number in the caption alongside a simple message in the box. (You may have to change the path-name to "iup32.dll" if you are using a 32-bit version of Euphoria.) </p>

<a name="_7_furtherdevelopment"></a><a name="furtherdevelopment"></a><h2>Further development</h2>
<p> The IUP libraries are very well documented, so finding out how to add further functionality to <em>iup.ew</em> should be reasonably straightforward. </p>

<p> This site has many examples and you can revise the C-language examples to Euphoria with minimal effort and relatively little knowledge of C. </p>

<p> The ease of developing Euphoria wrappers for other shared libraries will critically depend on the quality of the documentation of the C-source. </p>

<p> The replication of C-function names in Euphoria wrappers is a convention not a rule. When devising one's own wrapper it is wise to think through the strategy before writing the detail, as sometimes other features of Euphoria (default parameter values, for example) may be more dominant in your final design than the organisation of the C-language source module. For example, in IUP there are three C-functions for accessing version information; in Euphoria a single routine could easily wrap all three into a general purpose routine. </p>

<a name="_8_usingstandardlibraries"></a><a name="usingstandardlibraries"></a><h2>Using standard Libraries</h2>
<p> Instead of using the dedicated <em>c.e</em> module, you could call upon two of the "standard" libraries issued with Open Euphoria: </p>
<ul><li><em>dll.e</em>
</li><li><em>machine.e</em>
</li></ul>
<p> where equivalent, but differently-named, routines are defined. The module <em>dll.e</em> also contains the full set of C-types. </p>
</body></html>