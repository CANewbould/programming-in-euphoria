<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter3.cr" -->
</head>
<body>

<a name="_0_chapter3dataandhowitisstored"></a><a name="chapter3dataandhowitisstored"></a><h1>Chapter 3: Data and how it is stored</h1>
<p> Computer programming is all about declaring and assigning data, carrying out a range of processes on it and, usually, reporting something (result, summary, progress, conclusion). So what exactly does "data" entail? </p>

<p> Euphoria has a very simple approach to data. Values are stored in memory slots, which are identified by names, these being allocated following a simple set of rules. These slots can be assigned for fixed values or for one which vary. The latter (variables) come in two kinds (Euphoria calls these <em>type</em>s): single values; multiple values. </p>

<p> That's that. Now let's look in detail at these various ideas. </p>

<a name="_1_permissiblenames"></a><a name="permissiblenames"></a><h2>Permissible names</h2>
<p>  What follows explains both the obligatory and the optional aspects to naming Euphoria entities (values and routines). </p>

<a name="_2_rules"></a><a name="rules"></a><h3>Rules</h3>
<p> Some names are reserved by Euphoria, for structures (<em>if..then..else..end if</em>, etc) and others for routines (<em>puts</em>, <em>length</em>, etc). A programmer should avoid using these. The full list of reserved words can be found <a class="external" href="https://openeuphoria.org/docs/lang_decl.html#_111_declarations">here</a>. </p>

<p> It should be noted, however, that Euphoria names are case-sensitive, so, for example, <em>length</em> cannot be used but <em>Length</em> can. </p>

<p> Just as there are limits on naming files, there are also restrictions on what can be included in a name in Euphoria. An identifier must begin with either a letter or an underscore. It can then be followed by one or more letters, digits or underscore characters. No other characters are allowed. Although there is no strict limit to the length of an identifier, ones of 30 characters or more are discouraged! </p>

<a name="_3_conventions"></a><a name="conventions"></a><h3>Conventions</h3>
<p> It is best at the beginning of your time as a Euphorian to adopt a coherent convention for naming the different types of Euphoria entities and to apply these consistently. Unfortunately, if you study the various user-developed libraries that have been created over the last 25 years, although you may well find internal consistency, different conventions have been adopted. </p>

<p> Euphoria, in the main, follows this convention: </p>
<ul><li>upper-case for constants, including the snake-case variant
</li><li>lower-snake-case for routine names
</li><li>lower-case for variable names
</li></ul>
<p> Camel-case is a popular alternative and many coders choose this and lower-snake-case for values and routines, although which way round is quite variable! Leading underscores are quite popular for naming elements of a code block which underpin some more significant, but related, element. </p>

<p> As this chapter develops, as well as describing the various aspects of coding, each element will be described with the naming convention adopted in this book. </p>

<p> Whatever approach you choose, it would be sensible to make a note of the full set of conventions and keep this note handy until the convention is second nature. </p>

<a name="_4_declarationsandassignments"></a><a name="declarationsandassignments"></a><h2>Declarations and assignments</h2>
<p> The act of assigning a name to a value is usually termed a <em>declaration</em>; the act of assigning a(n initial) value to the named item of data is termed <em>assignment</em>. In Euphoria an assignment may be made in the same statement as a declaration but is not obligatory. </p>

<p>  So, for example, the following two code snippets yield the same result: </p>

<pre class="examplecode"><font color="#0000FF">atom </font><font color="#330033">a = 13/7</font>
</pre>

<p> or  </p>

<pre class="examplecode"><font color="#0000FF">atom </font><font color="#330033">a a = 13/7</font>
</pre>

<p>  (This latter approach was the only one possible in earlier versions of Euphoria, and so you might well find it still being used in up-to-date code. Another restriction in early versions of Euphoria was to require in a code block all declarations to be made before any assignment.) </p>

<p>  Equally you can apply the same approaches when declaring and initialising several variables together. For example: </p>

<pre class="examplecode"><font color="#0000FF">integer </font><font color="#330033">i = 16, j, k = -12, l</font>
<font color="#330033">... </font>
<font color="#330033">j = ... </font>
<font color="#330033">... </font>
<font color="#330033">l = ...</font>
</pre>

<p> shows a mix-and-match approach to declaration and assignment. </p>

<p>  The act of declaration creates a memory slot for the storage of the data and immediately associates that slot with the declared name. </p>

<p> The act of assignment, however, places a stated value in that memory slot. </p>

<p> How does Euphoria know how big to make the memory slot? How does it know what value(s) it is allowed to place there? </p>

<a name="_5_constants"></a><a name="constants"></a><h2>Constants</h2>
<p> In Euphoria a fixed value is known as a <strong>constant</strong>. A <strong>constant</strong> value is always declared and defined in a single statement. For example, </p>

<pre class="examplecode"><font color="#0000FF">constant </font><font color="#330033">TRUE = </font><font color="#880033">(</font><font color="#330033">1=1</font><font color="#880033">)</font>
</pre>

<p> Here, using the "=" as the assignment operator, we set the fixed value <em>TRUE</em> to the value returned when the number <em>1</em> is compared to itself. </p>

<p> The naming convention for fixed values used in this book is what is known as UPPER_CASE_SNAKE_CASE. Such a name is either a simple UPPER_CASE name, or where more than one name is used, each element is linked by an underscore. </p>

<p> As that value cannot change, Euphoria can easily work out the amount of space required and can ensure that the slot is (write-)protected for as long as the slot is required. </p>

<a name="_6_variables"></a><a name="variables"></a><h2>Variables</h2>
<p> The same cannot, of course, be said for a variable value slot, known in Euphoria simply as a <em>variable</em>. In order to find out how much space to reserve the programmer must incorporate into the act of declaration some indication of the space required. Euphoria also required that details of acceptable values are included in the declaration. </p>

<p> Fortunately, both of these can be achieved by declaring the <em>type</em> of a variable, as well as giving it a name. Euphoria has, in essence, just two data types: the <strong>atom</strong> and the <strong>sequence</strong>. </p>

<a name="_7_atoms"></a><a name="atoms"></a><h3>Atoms</h3>
<p>  Atoms are single-valued (numerical) variables, but of effectively any size. An <strong>atom</strong> value can either be a whole number or a decimal value. These are the limits: </p>
<ul><li>whole-number values in the range +/- power(2,53)
</li><li>floating point numbers in the range power(2,1024)+1 to +power(2,1024)-1
</li></ul>
<p>  Atoms can be operated on in a variety of ways. The arithmetic operators (<em>+</em>, <em>-</em>, <em>*</em> and <em>/</em>) all can be used to manipulate <strong>atom</strong> values, as can their more direct relations: (<em>+=</em>, <em>-/=/, </em>*=<em> and /</em>=<em>. So: </em></p>

<pre class="examplecode"><font color="#0000FF">atom </font><font color="#330033">a =14</font>
<font color="#330033">a *= 3	</font><font color="#FF0055">-- stores 42 in a</font>
</pre>

<p> changes the value of a variable directly, instead of the "re-assignment" equivalent: </p>

<pre class="examplecode"><font color="#0000FF">atom </font><font color="#330033">a =14</font>
<font color="#330033">a = 3 * a	</font><font color="#FF0055">-- stores 42 in a</font>
</pre>

<p> By default an <strong>atom</strong> is a base 10 numeral, but it is possible to specify a value as a binary, an octal, or as a hexadecimal. These specifications do not, however, affect how the value is actually stored, but act as conveniences of assignment. Examples are: </p>

<pre class="examplecode"><font color="#330033">0b101 </font><font color="#FF0055">--&gt; decimal 5</font>
<font color="#330033">0t101 </font><font color="#FF0055">--&gt; decimal 65 </font>
<font color="#330033">0d101 </font><font color="#FF0055">--&gt; decimal 101 </font>
<font color="#330033">0x101 </font><font color="#FF0055">--&gt; decimal 257</font>
</pre>

<p> An alternative way of expressing hexadecimals is to use the <em>#</em> prefix, so, for example: </p>

<pre class="examplecode"><font color="#330033">#FE             </font><font color="#FF0055">-- 254</font>
<font color="#330033">#A000           </font><font color="#FF0055">-- 40960 </font>
<font color="#330033">#FFFF00008      </font><font color="#FF0055">-- 68718428168 </font>
<font color="#330033">-#10            </font><font color="#FF0055">-- -16</font>
</pre>

<a name="_8_sequences"></a><a name="sequences"></a><h3>Sequences</h3>
<p>  Sequences are multi-valued variables, represented in Euphoria inside curly brackets {}. A <strong>sequence</strong> value can contain no elements ({} represents the "empty sequence") or as many as you can define until you run out of memory - and that is a very large number indeed. </p>

<p> The elements can be any form of <strong>object</strong> and can be nested to any depth (again within the memory limits of the machine). So, for example: </p>

<pre class="examplecode"><font color="#880033">{</font><font color="#330033">1, </font><font color="#330033">"two"</font><font color="#330033">, </font><font color="#993333">{</font><font color="#330033">3,4,5</font><font color="#993333">}</font><font color="#330033">, </font><font color="#993333">{</font><font color="#330033">6,7,</font><font color="#330033">"eight"</font><font color="#330033">, </font><font color="#0000FF">{</font><font color="#330033">1,2,3, </font><font color="#5500FF">{</font><font color="#330033">4,5,6,</font><font color="#00FF00">{</font><font color="#330033">7,8,9</font><font color="#00FF00">}</font><font color="#5500FF">}</font><font color="#0000FF">}</font><font color="#993333">}</font><font color="#880033">}</font>
</pre>

<p> is a perfectly valid sequence. It has four elements (it is said to be of length 4) as follows: </p>
<ol><li>an <strong>atom</strong>
</li><li>a <strong>sequence</strong>, in the form of a text string, of length 3
</li><li>a <strong>sequence</strong>, also of length 3, with <strong>atom</strong> elements
</li><li>a <strong>sequence</strong>, of length 4, made up of
</li></ol><ol><li>an <strong>atom</strong>
</li><li>an <strong>atom</strong>
</li><li>a <strong>sequence</strong>, in the form of a text string, of length 5
</li><li>a <strong>sequence</strong>, of length 4
</li></ol>
<p> This final element is made up of </p>
<ol><li>an <strong>atom</strong>
</li><li>an <strong>atom</strong>
</li><li>an <strong>atom</strong>
</li><li>a <strong>sequence</strong>, of length 4 made up of
</li></ol><ol><li>an <strong>atom</strong>
</li><li>an <strong>atom</strong>
</li><li>an <strong>atom</strong>
</li><li>a <strong>sequence</strong> of length 3, with <strong>atom</strong> elements
</li></ol>
<p> It is worth studying this until it is perfectly clear. </p>

<p> Section 5.2 of the Open Euphoria Manual explains how Euphoria stores and manages its fundamental types; it isn't at all necessary, however, that you understand any of this. </p>

<p> You "point at" an element using a square-bracket notation. So if the above value was assigned to a variable <em>s</em> then s[2] is equal to "two", as that is the second element in the sequence. By the same token s[2][3] is equal to 'o' - the third element inside the second element. It is, however, stored as <em>111</em>, the ascii value for 'o'. Euphoria always STORES characters in this way, although you retain flexibility over how you display things. So, for example: </p>

<pre class="examplecode"><font color="#0000FF">puts</font><font color="#880033">(</font><font color="#330033">1, s</font><font color="#993333">[</font><font color="#330033">2</font><font color="#993333">]</font><font color="#880033">)	</font><font color="#FF0055">-- displays "two"</font>
</pre>

<p> Let's consider going deeper. The element s[4] is the complicated one; s[4,4] is the last element of this and s[4][4][4] is {4,5,6,{7,8,9}. So s[4][4][4][4] is {7,8,9} and s[4][4][4][4][3] = 9, which is stored as an <strong>atom</strong>, with the value <em>9</em>. </p>

<p> A very similar syntax is used to extract a <em>slice</em> of a <strong>sequence</strong>. The slice s[2..3] produces the value {"two", {3,4,5}}, that is: that part of the sequence containing the second and third elements. Equally s[2][2..3] produces {119,111} the ascii codes for the 'w' and 'o' of "two". </p>

<p> A neat shorthand converts the clumsy s[3..length(s)] into s[3..$]. It even works like this: s[2..$-1]. </p>

<p> There are a number of built-in routines for manipulating <strong>sequence</strong>s, which will be looked at in Chapter 6. </p>

<a name="_9_integer"></a><a name="integer"></a><h3>Integer</h3>
<p> Integers are whole number values in the range -1,073,741,824 to +1,073,741,823. Euphoria's Core incorporates an <strong>integer</strong> type, which allows for very fast computation of whole numbers. </p>

<p> It should be recognised, however, that the square of an <strong>integer</strong>, or the product of two <strong>integer</strong>s could exceed the integer limit, so would need an <strong>atom</strong> assignment to avoid an error. So always use <strong>atom</strong>s in preference to <strong>integer</strong>s unless you are confident that all your values (now and in future use) fit the range. </p>

<a name="_10_object"></a><a name="object"></a><h3>Object</h3>
<p>  If, for any reason, you want a variable that can be either an <strong>atom</strong> or a <strong>sequence</strong> then simply declare it to be an <strong>object</strong>. </p>

<p> A good example of such an assignment is when reading from a file of text. A text file consists of line-after-line of text (<strong>sequence</strong>s) terminated by an end-of-file marker, which is an <strong>atom</strong>. </p>

<a name="_11_handles"></a><a name="handles"></a><h3>Handles</h3>
<p> Although not strictly about types, we need to reflect upon a concept which is quite fundamental to how Euphoria works, although little is written to indicate this: the <em>handle</em>. </p>

<p> By handle I mean the utilisation of a whole-number value to act as a direct representation of a wide range of programming concepts. </p>

<p> In Euphoria handles are used in a variety of ways, as will become clear in later chapters. Input/Output devices (keyboard, screen, files, etc) are represented by small integers (0, 1, 2, 3, ...), as will be covered in the next chapter. </p>

<p> A parallel set of handles (starting at 0 and incrementing) is used to represent Euphoria routines when a routine is called indirectly (see Chapter 11 for the development of this idea.) </p>

<p> In both these cases it is part of the Euphoria Interpreter's task to manage the assignment of such handles, as well as to collect-up and tidy at end of use, through Euphoria's efficient "garbage collection". The user simple accepts the handle values which Euphoria assigns and uses that assignment to interact and manipulate with what the handle represents. </p>

<p> Another use of handles occurs when we want to incorporate C-program routines into Euphoria code. Every C routine is assigned a handle when defined in Euphoria (see Chapter 17 for full details of how all this works), although here the values can be so large that one may need to declare an <strong>atom</strong> variable to hold them! (They will always be whole-number values, though.) </p>

<p> Programming using a Graphical User Interface (GUI) is based on defining "widgets" (visual constructs like buttons, windows, even menu items), their relationship to each other (for example, which window a button should sit in), and how to respond to user "events" (clicking the mouse, chosing a menu item, closing a window, etc). In this context handles are used for widgets and, in some GUI implementations, for events as well. </p>

<a name="_12_thespecialcaseof1"></a><a name="thespecialcaseof1"></a><h3>The (special) case of -1</h3>
<p> Euphoria has a habit of using the <em>-1</em> code as a trap (condition) for a range of different circumstances. For example, it can convey the fact that a file cannot be found, or opened; the end of a stream of data has been reached; some process has terminated unexpectedly; or some expected circumstance has not occurred. We shall come across some of these usages as we go through this e-book. Programmers may also find it useful, when developing their own routines, to use a similar strategy. </p>

<a name="_13_next"></a><a name="next"></a><h2>Next</h2>
<p> To find out how we can input or output our data read <a href="chapter4.html">on</a>. </p>
</body></html>