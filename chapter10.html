<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter10.cr" -->
</head>
<body>

<a name="_0_chapter10modularity"></a><a name="chapter10modularity"></a><h1>Chapter 10: Modularity</h1>
<a name="_1_whatisamodule"></a><a name="whatisamodule"></a><h2>What is a module?</h2>
<p> A module is a self-contained block of code stored in a file, and, in Euphoria, tagged by an extension of either "e" or "ew"; the latter being used if any of the functionality is graphical. This convention is not always adhered to by Euphoria programmers, even those who are part of the Open Euphoria development team! </p>

<p> A module is not necessarily a program; that is, one which contains a call to a routine and/or an executable (sub) block of code. To aid the distinction between a library module and an application, in this book a convention is adopted of defining an application as one containing both the definition of a <tt>main</tt> routine and the calling of it, on at least one occasion. </p>

<a name="_2_whyusemodules"></a><a name="whyusemodules"></a><h2>Why use modules?</h2>
<p> There are several good reasons for adopting a modular approach to programming, in general, and in the case of Euphoria in particular. A few are described below. </p>

<p> It is often the case that a programmer wants to do the same (sort of) things in more than circumstance. Instead of re-coding, copy-pasting, etc, the sensible approach is to place this common code in a separate module and refer to it (by means explained below) as and when needed. </p>

<p> Modularisation encourages generalisation. (This is the basic argument behind the OOP philosophy!) Adding parameters to routines, for example, can expand the utility of that routine to meet newly-discovered variations on the original theme. Not only is generalisation made easier by modularisation, but modularisation itself encourages programmers to think in more general terms and thus, hopefully, find more useful solutions. </p>

<p> Modularisation encourages code separation. It is not uncommon to find a large application using the same variables in lots of different places, often not for the same reason, out of "economy" rather than logic. This can often lead to error, at worst, or confusion, at best, when another developer tries to modify the code. By creating smaller blocks of code everything is more self contained, variable use is more refined and error-detection rendered more efficient. Additionally this approach encourages a more thoughtful consideration of how best to use variables overall and for each and every internal code block. </p>

<p> Above all modularisation encourages "small". You can find recommendations in programming guides which suggest that smaller modules are the better, especially if development is being shared, or longelevity is a key consideration. An optimum figure of 50 lines of code has been suggested. </p>

<p> Modularity encourages documentation. Because the process of documentation is often thought of as a chore, there is a temptation to be as skimpy as possible when writing documentary comment inside an application. But when creating a module, even if the original intention is solely for ones own use, the likelihood of it being re-used is greatly enhanced if there is documentation available describing the module's interface. </p>

<p>   Modularisation aids development. Because a module is free-standing, it can be enhanced or modified without necessarily causing any impact on other modules. Provided the revised interface, after modification, doesn't conflict with its predecessor there are no knock-on effects. One aspect of Euphoria which makes this much more likely is the ability to assign default values to arguments so that a routine's argument list can be extended without impact on other modules which were designed to call upon an earlier version of the routine. </p>

<a name="_3_theincludekeyword"></a><a name="theincludekeyword"></a><h2>The include keyword</h2>
<p> In earlier chapters we have seen some references to the word <em>include</em> but now we get serious about it. It is a Euphorian keyword - a reserved work in the language. If in any code block you want to refer to a code block which is external to the current file/module then the keyword followed by the name of the module containing the desired code block, within certain rules - see the next section, ensures that the elements of the code block are available in the "calling" module. </p>

<p> The <em>include</em> keyword can be prefaced by another keyword, <em>public</em>, which is the means whereby this form of access can be inherited. To understand this better we need to address, in much greater detail than we have hitherto, the whole concept of <em>scope</em> in Euphoria. </p>

<a name="_4_scope"></a><a name="scope"></a><h3>Scope</h3>
<p> The word "scope" is the technical term for visibility. Whenever a value is declared or a type or routine defined it automatically has, built into the core of Euphoria, a "visible life". That visibility, by default is <em>local</em>, which means it exists solely within the code block in which it is introduced. (Remember here the discussion, in Chapter 7, about looping variables where their scope expires the moment a <em>for</em> loop meets the end condition, but where the variable would continue to have a life after a <em>while</em> loop is exited.) </p>

<p> To make anything visible outside the "home" module we need to "qualify" the scope by adding one of three keywords immediately before the declaration/definition. These scope qualifiers are: </p>
<ul><li>export
</li><li>public
</li><li>global
</li></ul>
<p> and they define a hierarchy of visibility. </p>

<p> The <em>export</em> scope passes on visibility just one level "up": if Module <em>b.e</em> includes Module <em>a.e</em> then any variable, type or routine declared to be <em>export</em>ed in module <em>a.e</em> can be "seen", and therefore used, in <em>b.e</em>. But if a third module, <em>c.e</em> includes <em>b.e</em> then these elements are invisible in <em>c.e</em>, unless this module also explicitly includes <em>a.e</em>. </p>

<p> The <em>public</em> scope could help in such cases because it allows for inheritance. Placing this keyword in front of values or routines allows <em>c.e</em> to see such items in <em>a.e</em> provided the intermediate module, <em>b.e</em>, qualifies the <em>include</em> thus: </p>

<pre class="examplecode"><font color="#FF0055">-- b.e</font>
<font color="#0000FF">public include </font><font color="#330033">a.e</font>
</pre>

<p> which means that all publicly-available material in <em>a.e</em> "passes through" all the way to anything which includes <em>b.e</em>, here <em>c.e</em>. </p>

<p> Moreover, the use of the <em>public</em> keyword enables inheritance to be chained. For example, if Module <em>c.e</em> incorporated <em>b.e</em> using: </p>

<pre class="examplecode"><font color="#FF0055">-- c.e</font>
<font color="#0000FF">public include </font><font color="#330033">b.e</font>
</pre>

<p> then any module calling <em>c.e</em> could gain access to all exported elements defined in <em>c</em>, and all publicly-defined elements in both <em>b</em> and <em>a</em>. </p>

<p> The distinction between <em>export</em> and <em>public</em> allows a coder to make conscious choices about visibility on a case-by-case basis; in the original definition of Euphoria there were only two scope states: local and <em>global</em>, which leads us to the third scope keyword. </p>

<p> Any value (constant or variable), type or routine (procedure or function) prefixed by the term <em>global</em> is visible not only in modules that directly call the parent module but also in any other module which co-exists within a run-time "collection". This may seem like a bit of a mouthful, but it means that such values are visible both directly (through a chain of <em>include</em>s) and indirectly (via any included module referred to in any other module). </p>

<p> Consequently, programmers are now discouraged from using this level of scope, whereas prior to Open Euphoria it was the only available method of widening scope. Legacy material may, therefore, contain <em>global</em> calls but if you want to use such modules it is probably advisable to edit the source and reset the scope to either <em>export</em> or <em>public</em>. There is, I think, one exception to this guidance. If you are designing a significant programming solution, using many supporting modules and all, or the majority, call upon the same user-defined element (probably a constant, a type or a routine) then a <em>global</em> declaration in the "core" module ensures visibility throughout the solution. This can save a lot of careful designation of <em>public</em> both with regard to elements and to modules. </p>

<a name="_5_programmingelements"></a><a name="programmingelements"></a><h3>Programming elements</h3>
<p> In Chapter 7 I reviewed my approach to laying out code blocks in general and modules (as we now know them) in particular. Whilst I do not intend to cover all that ground again, there are a few aspects worthy of repeat. Some languages have a concept of an "interface" where only those elements which have external scope are laid out. I like this concept and try to implement it the layout of code and in the documentation style I use. </p>

<p> As argued above, a number of small dedicated modules is better than one big one. I view a solution as being made up of one small main module, containing the <tt>main</tt> procedure and only the minimum of local elements. Supporting it and enabling the solution are "library" module. I define a library module as one without a <tt>main</tt> but which has some elements with external scope - perhaps constants, routines and/or types but without variables, as these are invariably problematic. Ideally these libraries should provide the most generalised utility, just in case they are re-usable in another solution. In that same vein, I tend to build the libraries hierarchically, using <em>export</em> and <em>public</em> to optimise usage whilst minimising redundancy. </p>

<a name="_6_next"></a><a name="next"></a><h2>Next</h2>
<p> We have, now, more or less covered the features of the Euphoria Core. Before leaving the Core, however, we should take a look at two useful features. First we look at <a href="chapter11.html">calling routines indirectly</a>. </p>
</body></html>