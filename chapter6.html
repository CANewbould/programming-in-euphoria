<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter6.cr" -->
</head>
<body>

<a name="_0_chapter6routines"></a><a name="chapter6routines"></a><h1>Chapter 6: Routines</h1>
<p>  Routines are used whenever repetitive processes are required. </p>

<p> A routine is a block of code which provides a consistent set of operations which are delivered through an interface. This provides the user with a window into the "black box" of the workings. The code inside the block is private, as far as a user is concerned, but the functionality can be accessed via the interface. The interface consists of a unique name, to identify the routine, and a set of arguments (from none upwards) which indicate the type and character of the values you can feed into the routine in order to vary the outcomes. In certain cases the name is prefixed by a scope-defining keyword (see Chapter 10 for details), which indicates the level of availability of the routine in other contexts. </p>

<p> Routines are invoked by calling the unique name followed by brackets, inside which are the values you want the routine to consider, consistent with the rules set out for the types of these parameters. </p>

<p> Lets define a very simple illustration and then test it a few times. Suppose we wish to keep adding pairs of numbers (integers and floating-point values) then the following code defines such a routine: </p>

<pre class="examplecode"><font color="#0000FF">procedure </font><font color="#330033">add</font><font color="#880033">(</font><font color="#0000FF">atom </font><font color="#330033">one, </font><font color="#0000FF">atom </font><font color="#330033">two</font><font color="#880033">)</font>
<font color="#0000FF">	printf</font><font color="#880033">(</font><font color="#330033">1, </font><font color="#330033">"%g + %g = %g\n"</font><font color="#330033">, </font><font color="#993333">{</font><font color="#330033">one, two, one + two</font><font color="#993333">} </font>
<font color="#0000FF">end procedure</font>
</pre>

<p> The routine's code block begins with the type of the routine (<em>procedure</em> or <em>function</em>) and ends with that same type-name, after the <em>end</em> keyword. In between, and usually indented for emphasis, is the routine's body, consisting of one or more code lines which perform the task implied by the details of the interface. The interface's action is fully described on the remainder of the first line, where we find the unique name, here <tt>add</tt>, and, in brackets, the type and working name of each argument, separated by commas. The first line is often called the <em>signature</em> of the routine, which is just an alternative term for <em>interface</em>. The working names act as proxies for the actual values which are passed into the code when the routine is activated. </p>

<p> We can then call the routine, by stating its name and embedding in brackets the value(s) we want the routine to consider. In our simple case we could call it several times: </p>

<pre class="examplecode"><font color="#330033">add</font><font color="#880033">(</font><font color="#330033">2, 3</font><font color="#880033">)</font>
<font color="#330033">add</font><font color="#880033">(</font><font color="#330033">13/7, 12*3</font><font color="#880033">) </font>
<font color="#330033">add</font><font color="#880033">(</font><font color="#330033">34, .089</font><font color="#880033">)</font>
</pre>

<p> Both conceptually and in practice each call of a routine involves a process of substitution: the variable <em>one</em> takes the first value, and <em>two</em> the second, the steps inside the routine are processes line-after-line, until <em>end</em> is reached and the interpreter returns from the first call to find another, then repeats until the last line. If you use the <em>trace</em> facility offered by Euphoria's Core then you can step through line-by-line and see just how Euphoria literally goes through and through the embedded code inside the routine's definition and that, in all cases, it leaves the routine to work on the next line of code. </p>

<a name="_1_functions"></a><a name="functions"></a><h2>Functions</h2>
<p> In Euphoria functions are routines which, by definition, return a value. Two very importance aspects of return values need to be understood for a proper grasp of the language. </p>

<a name="_2_thetype"></a><a name="thetype"></a><h3>The type</h3>
<p> Many languages require the programmer to specify, in a routine's signature, the type of the return value. Not only is there no requirement for this in Euphoria but also, in standard Euphoria, no formal mechanism for doing so. (In Chapter 26 we come across a language extension which offers a mechanism, but does not require its implementation. Contrast, too, the case of the quasi-function <em>type</em> definitions, where the type of the return value IS pre-determined - a boolean TRUE/FALSE.) </p>

<p> As a programmer you need to consider whether this aspect matters to you. If it does, then the simple solution is, for all your own definitions, to declare, inside the function, a variable of the type you wish the return value to be, and operate exclusively on that variable before returning it. This way type-checking becomes part of the definition rather than something passed on to the future user (who may well not be you) to discover at run-time. </p>

<p> Whether or not you adopt such a strategy, you will need also to think about ensuring that your documentation clarifies the return-value's type and any special values being used within that type. One simple aid is to add a comment to the function's signature line declaring the return-value type, along with notes of any special cases. For example: </p>

<pre class="examplecode"><font color="#330033">&lt;built-in&gt;</font><font color="#0000FF">function length</font><font color="#880033">(</font><font color="#0000FF">object </font><font color="#330033">this</font><font color="#880033">)	</font><font color="#FF0055">-- the (integer) length of the object (atom -&gt; 1)</font>
</pre>

<a name="_3_passingbyvalue"></a><a name="passingbyvalue"></a><h3>Passing by value</h3>
<p> A key aspect of Euphoria concerns the nature of all routines' processing. Whatever values are passed, as parameters, into a routine (procedures and functions alike) are unaffected by anything that happens inside the routine's definition, even if it seems that the parameter's value is being changed. Look at this definition: </p>

<pre class="examplecode"><font color="#0000FF">function insert</font><font color="#880033">(</font><font color="#0000FF">sequence </font><font color="#330033">this, </font><font color="#0000FF">object </font><font color="#330033">that, </font><font color="#0000FF">integer </font><font color="#330033">place</font><font color="#880033">)</font>
<font color="#330033">	this = this</font><font color="#880033">[</font><font color="#330033">1..place-1</font><font color="#880033">] </font><font color="#330033">&amp; that &amp; this</font><font color="#880033">[</font><font color="#330033">place..$</font><font color="#880033">] </font>
<font color="#0000FF">	return </font><font color="#330033">this </font>
<font color="#0000FF">end function</font>
</pre>

<p> It appears that the first parameter value is being changed inside the function code block before it is returned. </p>

<p> When called, however, for example by: </p>

<pre class="examplecode"><font color="#0000FF">sequence </font><font color="#330033">s = </font><font color="#880033">{</font><font color="#330033">1,2,3,4,5</font><font color="#880033">}</font>
<font color="#330033">...</font><font color="#0000FF">insert</font><font color="#880033">(</font><font color="#330033">s, 6, 3</font><font color="#880033">) </font>
<font color="#330033">?s</font>
</pre>

<p> you will see that the source <em>s</em> keeps its value UNLESS the function's return value is assigned (back) to <em>s</em>. </p>

<p> In Euphoria parameters are said to be "passed by value". Core Euphoria does not have the ability to "pass by reference", an approach which does allow for possible changes to passed variables, although several extension libraries (see later) offer this approach. </p>

<a name="_4_returnvalues"></a><a name="returnvalues"></a><h3>Return values</h3>
<p> A function can return any sort of Euphoria value: <strong>atom</strong> or <strong>sequence</strong>. If the latter then it is up to the programmer using such a function to strip out the component parts, if that is necessary for effective processing. (This is not always the case, as, for example, a function defined to create a series of integers has already delivered the desired result!) </p>

<p> Functions can, and often do, have more that one exit point. All such points must be defined by the <em>return</em> keyword and be followed by the value (or expression) that the programmer wishes to associate with the exit condition. When defining a function it is important to ensure that, as well as any exit points you include via <em>return &lt;expression&gt;</em> statements, you ensure that the "fall-through" position (reaching the end of the code block without <em>return</em>ing) is also covered. For example, the following code, taken from the standard <em>sort</em> library has four specific return/exit points plus the fall-through state - annotated with comment in the extract below. </p>

<pre class="examplecode"><font color="#0000FF">function </font><font color="#330033">column_compare</font><font color="#880033">(</font><font color="#0000FF">object </font><font color="#330033">a, </font><font color="#0000FF">object </font><font color="#330033">b, </font><font color="#0000FF">object </font><font color="#330033">cols</font><font color="#880033">)</font>
<font color="#FF0055">-- Local function used by sort_columns() </font>
<font color="#0000FF">	integer </font><font color="#330033">sign </font>
<font color="#0000FF">	integer </font><font color="#330033">column </font>
<font color="#0000FF">	for </font><font color="#330033">i = 1 </font><font color="#0000FF">to length</font><font color="#880033">(</font><font color="#330033">cols</font><font color="#880033">) </font><font color="#0000FF">do </font>
<font color="#0000FF">		if </font><font color="#330033">cols</font><font color="#880033">[</font><font color="#330033">i</font><font color="#880033">] </font><font color="#330033">&lt; 0 </font><font color="#0000FF">then </font>
<font color="#330033">			sign = -1 </font>
<font color="#330033">			column = -cols</font><font color="#880033">[</font><font color="#330033">i</font><font color="#880033">] </font>
<font color="#0000FF">		else </font>
<font color="#330033">			sign = 1 </font>
<font color="#330033">			column = cols</font><font color="#880033">[</font><font color="#330033">i</font><font color="#880033">] </font>
<font color="#0000FF">		end if </font>
<font color="#0000FF">		if </font><font color="#330033">column &lt;= </font><font color="#0000FF">length</font><font color="#880033">(</font><font color="#330033">a</font><font color="#880033">) </font><font color="#0000FF">then </font>
<font color="#0000FF">			if </font><font color="#330033">column &lt;= </font><font color="#0000FF">length</font><font color="#880033">(</font><font color="#330033">b</font><font color="#880033">) </font><font color="#0000FF">then </font>
<font color="#0000FF">				if not equal</font><font color="#880033">(</font><font color="#330033">a</font><font color="#993333">[</font><font color="#330033">column</font><font color="#993333">]</font><font color="#330033">, b</font><font color="#993333">[</font><font color="#330033">column</font><font color="#993333">]</font><font color="#880033">) </font><font color="#0000FF">then </font>
<font color="#0000FF">					return </font><font color="#330033">sign * eu:</font><font color="#0000FF">compare</font><font color="#880033">(</font><font color="#330033">a</font><font color="#993333">[</font><font color="#330033">column</font><font color="#993333">]</font><font color="#330033">, b</font><font color="#993333">[</font><font color="#330033">column</font><font color="#993333">]</font><font color="#880033">) </font><font color="#FF0055">-- (1) </font>
<font color="#0000FF">				end if </font>
<font color="#0000FF">			else </font>
<font color="#0000FF">				return </font><font color="#330033">sign * -1 </font><font color="#FF0055">-- (2) </font>
<font color="#0000FF">			end if </font>
<font color="#0000FF">		else </font>
<font color="#0000FF">			if </font><font color="#330033">column &lt;= </font><font color="#0000FF">length</font><font color="#880033">(</font><font color="#330033">b</font><font color="#880033">) </font><font color="#0000FF">then </font>
<font color="#0000FF">				return </font><font color="#330033">sign * 1 </font><font color="#FF0055">-- (3) </font>
<font color="#0000FF">			else </font>
<font color="#0000FF">				return </font><font color="#330033">0 </font><font color="#FF0055">-- (4) </font>
<font color="#0000FF">			end if </font>
<font color="#0000FF">		end if </font>
<font color="#0000FF">	end for </font>
<font color="#0000FF">	return </font><font color="#330033">0 </font><font color="#FF0055">-- (5) </font>
<font color="#0000FF">end function</font>
</pre>

<p> A common "error" in programming (strictly a <em>warning</em> in Euphoria) is to place line(s) of code after a <em>return</em> statement, as such lines can NEVER be executed ("reached"). So, if you want, for example, to issue a message associated with a particular exit point, then either place the message call immediately BEFORE the <em>return</em> statement, or add to the calling code a "trap" whereby you interrogate the return value and, if it is the desired one, then issue the message there. (Note that, for this second strategy to work you must have unique return values for each exit.) </p>

<p> Euphoria's Core allows the programmer to choose whether or not to trap a function's return value; earlier versions of the language core didn't. There are two such obvious cases: </p>
<ol><li>when the value is used to test a condition (in <em>if</em> or <em>while</em> statements, for example) and has been defined as part of testing, but isn't needed after proving the routine
</li><li>when the function plays a dual role (sometimes acting like a function, sometimes like a procedure) and the latter context applies. (A good example is the message_box function, which all GUI implementations offer. Here the return value is a signal indicating which button inside the message box was clicked by the user. Sometimes a programmer wants to develop this further, but often (s)he does not.)
</li></ol>
<a name="_5_functionsascreators"></a><a name="functionsascreators"></a><h3>Functions as creators</h3>
<p> In normal circumstances, all values returned by functions are assigned, and, as indicated above, must be consistent with the type of the receiving value, if the assignment is to a variable. As such, many Euphoria functions can be seen as <em>creator</em>s: they manufacture a new value, without, in any way, affecting the values of any other pre-defined values, including any which may have been passed as parameters to the function - see above. </p>

<p> This role is central to many programming situations, especially ones where "handles" are being used to associate a variable with a pointer (integer or atom) - to a memory slot, library or externally-defined function. </p>

<p> As indicated above, although functions can be creators, they can also be used to report on (locally-defined) values stored within a code block (technically known as <em>accessors</em>). They cannot, however, be defined as changers (technical term <em>mutator</em>s); when you re-assign the source's value with a return value from a function call, that change takes place OUTSIDE the body of the function and not within it! </p>

<a name="_6_procedures"></a><a name="procedures"></a><h2>Procedures</h2>
<p> Procedures are simply routines without return values! In some programming languages they are known as "void" functions. In terms of structure they are similar to functions, even to the point of recognising the <em>return</em> keyword!  </p>

<p> When <em>return</em> is used in a procedure, however, although the same effect occurs (leaving the routine's code block and positioning at the start of the next code block)there is no provision for associating a value with the exit point. Equally, there is no need to include <em>return</em> as the last line in a procedural code block, as fall-through is automatic in Euphoria. </p>

<a name="_7_defaultparameters"></a><a name="defaultparameters"></a><h2>Default parameters</h2>
<p> A really useful feature, introduced with the Open Euphoria Version (v4.0 onwards), is the ability to define, in a routine's signature, default values: values to be assumed if not "over-written" when called. For example, the signature of the <tt>find</tt> function reads as follows: </p>

<pre class="examplecode"><font color="#330033">&lt;built-in&gt; </font><font color="#0000FF">function find</font><font color="#880033">(</font><font color="#0000FF">object </font><font color="#330033">needle, </font><font color="#0000FF">sequence </font><font color="#330033">haystack, </font><font color="#0000FF">integer </font><font color="#330033">start = 1</font><font color="#880033">)</font>
</pre>

<p> which means that the function appears to have two states: </p>

<pre class="examplecode"><font color="#330033">&lt;built-in&gt; </font><font color="#0000FF">function find</font><font color="#880033">(</font><font color="#0000FF">object </font><font color="#330033">needle, </font><font color="#0000FF">sequence </font><font color="#330033">haystack</font><font color="#880033">)</font>
</pre>

<p> which institutes the most common search condition - scanning the complete source (the <em>haystack</em>) for a (the first) match - and </p>

<pre class="examplecode"><font color="#330033">&lt;built-in&gt; </font><font color="#0000FF">function find</font><font color="#880033">(</font><font color="#0000FF">object </font><font color="#330033">needle, </font><font color="#0000FF">sequence </font><font color="#330033">haystack, </font><font color="#0000FF">integer </font><font color="#330033">start</font><font color="#880033">)</font>
</pre>

<p> where a starting position within the source is defined. </p>

<p> Before Open Euphoria's development there were two functions for these operations (<tt>find</tt> and <tt>find_from</tt>) to meet both circumstances. </p>

<p> This process (of defining multiple potential situations within a single signature) is known as polymorphism and is generally perceived to be a most desirable aspect of modern-day programming. </p>

<a name="_8_postscipt"></a><a name="postscipt"></a><h2>Postscipt</h2>
<p> Although it may not seem obvious, a lot of things in Euphoria's Core are, fundamentally, routines. To the <a class="external" href="https://openeuphoria.org/docs/builtins.html#_783_builtinmethods">built-ins</a>, detailed in the Open Euphoria Manual, could be added the relational operators, logical operators, arithmetic operators and sequence operators, which, although typically taking a different form: </p>

<p> 
<p>&lt;arg1&gt; <tt>&lt;op&gt;</tt> &lt;arg2&gt;<br /> </p>
 </p>

<p> are entirely analagous to a functional definition: </p>

<p> 
<p>global <tt>&lt;op&gt;</tt>(&lt;arg1&gt;, &lt;arg2&gt;)<br /> </p>
 </p>

<p> If this seems a bit theoretical then, by all means ignore it, but if you want to think it through then try imagining that: </p>

<p> 
<p>? (a + b)<br /> </p>
 </p>

<p> means: </p>

<p> 
<p>? <tt>add</tt>(a, b)<br /> </p>
 </p>

<p> 
<p>? not this<br /> </p>
 </p>

<p> means: </p>

<p> 
<p>? <tt>not</tt>(this)<br /> </p>
 </p>

<p> or, even, </p>

<p> 
<p>? s &amp; t<br /> </p>
 </p>

<p> meaning </p>

<p> 
<p>? <tt>concat</tt>(s, t)!<br /> </p>
 </p>

<a name="_9_next"></a><a name="next"></a><h2>Next</h2>
<p> The <a href="chapter7.html">next chapter</a> covers program flow. </p>
</body></html>