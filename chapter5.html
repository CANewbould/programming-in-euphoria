<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter5.cr" -->
</head>
<body>

<a name="_0_chapter5addingcommentstoeuphoriacode"></a><a name="chapter5addingcommentstoeuphoriacode"></a><h1>Chapter 5: Adding comments to Euphoria code</h1>
<p>  All languages offer some means of embedding comment into blocks of code. </p>

<p> Comment is useful in a variety of ways. It can remind the original coder of particular issues in devising the code which might not be remembered when the code is studied by that same author in the future. It can act as a guide to other coders, which is especially useful when a team is involved in program development. It can also be useful in allowing the actual documentation (that which is made available to users - these days usually in the form of an <em>html</em> file) to be embedded inside the code module itself. (Some languages do this automatically; in the case of Euphoria it involves a process of extraction and/or conversion. Strategies for doing this are discussed later in this chapter.) </p>

<a name="_1_inlinecomment"></a><a name="inlinecomment"></a><h2>In-line comment</h2>
<p> In Euphoria any line that begin with two dashes (<em>--</em>) is treated as comment. Moreover on any line of code containing <em>--</em>, everything after the dashes is also treated as comment. </p>

<a name="_2_blockcomment"></a><a name="blockcomment"></a><h2>Block comment</h2>
<p> It is also possible to comment out whole blocks within the code block, using a simple bracketing system: starting with <em>/*</em> and ending with <em>*/</em>. This is useful in two ways: it provides a way of writing extensive comments without the need to "paginate"; when developing new code it is sometimes useful to isolate areas of code that the programmer is less confident about. Here commenting out the problemmatic block (and perhaps narrowing it as development progresses) can sometimes save a lot of time - see Chapter 8 for a more complete analysis of this strategy. </p>

<a name="_3_commentasdocumentation"></a><a name="commentasdocumentation"></a><h2>Comment as documentation</h2>
<p> As indicated above, comment can also be used as part of the process of generating documentation. Traditionally program development involved writing the code modules (something coders are good at) and, separately, writing in a different language/format the instructions for use (something probably seen as a bit of a chore). Changes in code invariable involve changes in the documentation, so adopting an in-code documentation strategy is quite a godsend. </p>

<p> Strictly speaking, what follows here is beyond the scope of Core Euphoria as both of these strategies employ extension strategies which are only introduced in Chapter 13. Perhaps, therefore, the reader might be inclined to skip the rest of this chapter and come back to it later. It would be a pity, however, if the notion of in-code documentation was not embraced at an early stage in learning the language. </p>

<p> The rest of this chapter describes three strategies for documentation production using in-code commenting. All use a very simple markup language, <em>creole</em> at some stage in the process. Thereafter the approaches differ somewhat, but, in each case the result is three files: </p>
<ul><li>the source code
</li><li>the creole version of the documentation
</li><li>the html version of the documentation
</li></ul>
<p> although the third file is only really optional in the second strategy. </p>

<a name="_4_theeudocprovision"></a><a name="theeudocprovision"></a><h3>The EuDoc provision</h3>
<p> In this strategy the source code is written first. Embedded within it, however, is comment, some of which is designated as "special" (and thus, distinguishable from any ordinary comment). This special comment constitutes the material which is to form the source code's documentation. The approach is detailed in full <a class="external" href="https://openeuphoria.org/docs/eudoc.html#_751_eudocsourcedocumentationtool">here</a> and so will not be repeated here. The principle, however, is to scan the source code with a process (here written in Euphoria) which extracts this specialised comment and thereby creates an intermediate file of (here <em>creole</em>) code. This file is then processed by the <em>creole.exe</em> application, which is included in Euphoria's distro, and turned into an <em>html</em> file. </p>

<a name="_5_theliterateprogrammingprovision"></a><a name="theliterateprogrammingprovision"></a><h3>The Literate Programming provision</h3>
<p> This strategy differs from EuDoc, although they share the same motive - of tying together coding and documentation. The essential difference is that the concept of Literate Programming starts with documentation and not the source code. </p>

<p> In the version I use the base file is one written in <em>creole</em> markup language. It lays out the full details of the intended module. Creole is essentially very readable, so this file can act, if desired, as the sole document of record. </p>

<p> The really smart trick in Literate Programming is that the source code is not composed as a separate document, but is embedded inside the creole file, using a very simple bracketing convention. So the finished item shows (and interleaves) both the documentation and the code - in the same place! </p>

<p> A simple pre-processor (see later for explanation of that strategy) has been provided in the Open Euphoria <em>demo\preproc</em> folder, which, when run, generates the working Euphoria "program" file from the creole file, by the simple device of "flipping" the bracketed code and the textual material, to make the code visible and converting the documentation into comment. </p>

<p> As an optional extra, by using the <tt>creole</tt> application, the original file can be "converted" into an <em>html</em> file. (Those still following might have noticed that in this approach the code and the documentation always stay together, which the proponents of the approach think is so very important!) </p>

<a name="_6_somegeneralobservationsaboutincodedocumentation"></a><a name="somegeneralobservationsaboutincodedocumentation"></a><h3>Some general observations about in-code documentation</h3>
<p> Two of the strategies outlined here are both to be found in an Open Euphoria distribution. </p>

<p>  There are other strategies available, including non-language-specific ones, of which <a href="www.doxygen.nl/index.html">Doxygen</a> is one of the best known. Most work in a manner akin to EuDoc, in that the source code contains specialised comment, which is extracted in one pass, and converted, in another, to generate documentation in some other file format. </p>

<p> What is fundamentally important, however, is not to assume that the process of updating source code and documentation is automatically synchronised. You should understand both the strengths and limitations of your chosen strategy, and act accordingly to ensure consistency. (The literate programming approach has an advantage here because the strategy is more foolproof!) </p>

<p> All such methods benefits from the use of templates, which provide a universal layout. This not only makes documentation more uniform module-to-module, but also provide a framework for structuring all Euphoria software. </p>

<a name="_7_postcript"></a><a name="postcript"></a><h2>Postcript</h2>
<p> I hope that this introduction to commenting and the uses to which it can be put will, indirectly, serve another purpose in your Euphoria education. Underlying all that commenting can enhance, is a more fundamental issue: the style of your programming, the consistency and layout of your approach can all add up to making "reading" your code easier and more natural. By adopting a single style, reading "hard" bits of code is bound to be easier if "easy" bits follow the same general approach. </p>

<p> "Style" here can mean a variety of thing, many of which cannot yet be described because our journey is far from complete. Looking, however, at chapter titles, especially from Chapter 13 onwards, might give you a clue about different directions you can take once the Core is mastered. Each, in its own way, shows a "style". </p>

<a name="_8_next"></a><a name="next"></a><h2>Next</h2>
<p> We now have the basic tools, so we can move on to study the core elements of the Core - starting with the concept of a <a href="chapter6.html">routine</a>.  </p>
</body></html>