<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter20.cr" -->
</head>
<body>

<a name="_0_chapter20databases"></a><a name="chapter20databases"></a><h1>Chapter 20: Databases</h1>
<p> We shall take a detailed look at Euphoria's database management provision, the Euphoria Database System(EDS), as provided in the module library <em>eds.e</em> in the Standard Library folder. </p>

<p> Later we shall outline other provision for accessing databases using other tools. </p>

<a name="_1_edsanintroduction"></a><a name="edsanintroduction"></a><h2>EDS - an introduction</h2>
<p> First we look at the theoretical structure of a database. (This text is largely the same as that contained in the original Euphoria documentation.) </p>

<p> In EDS, a database is a single file with an <em>edb</em> extension. An EDS database is a container device: it holds tables. Each created table has a name, and contains records. Each record consists of two parts: the <em>key</em> and the <em>data</em>. The key can be any Euphoria object - an atom, a sequence, a deeply-nested sequence, whatever. Similarly the data can be any Euphoria object. There are no constraints on the size or structure of either the key or the data. It is crucial to note, however, that within a given table, each keys must be unique. That is, no two records in the same table can have the same key part. </p>

<p> The records in a table are stored in ascending order of key value. An efficient binary search is used when you refer to a record by its key. You can also access a record directly, with no search, if you know its current record number within the table. Record numbers are integers from 1 to the current number of records in the table. By incrementing the record number, you can efficiently step through all the records, in order of key. Note however that a record's number is dynamic: it can change whenever a new record is inserted, or an existing record is deleted. In contrast key-based searching is fixed. </p>

<p> The keys and data parts are stored in a compact form, but no accuracy is lost when saving or restoring floating-point numbers or any other Euphoria data. </p>

<p> The standard library works regardless of Operating System, so that a database created in one environment can equally be accessed and developed in another. </p>

<p> Unlike most other database systems, an EDS record is not required to have either a fixed number of fields, or fields with a preset maximum length. </p>

<p> It is entirely up to the user to determine the system of naming keys and to ensure that all keys fit within the chosen pattern. There are no fixed rules or limitations on the choice of schema. If there is no obvious naming rule, then simply create a unique integer value to be the key. Remember that you can always access the data by record number. It is easy to loop through the records looking for a particular field value. </p>

<p> A fundamental notion in EDS is that of the <em>current</em> state. Whilst the user may have more than one database, only one can be active at any one time. Likewise, only one table within the current database can be active. </p>

<a name="_2_creatingorselectingadatabase"></a><a name="creatingorselectingadatabase"></a><h3>Creating or selecting a database</h3>
<p> To create a new EDS database use the routine <em>db_create</em>. Its signature is: </p>

<pre class="examplecode"><font color="#0000FF">integer </font><font color="#330033">i1 = db_create</font><font color="#880033">(</font><font color="#330033">path, lock_method, init_tables, init_free</font><font color="#880033">)</font>
</pre>

<p> where the parameters give, respectively, a path name for the new database (the <em>edb</em> extension will be added if nothing is offered); a boolean value (<em>DB_LOCK_NO</em> - the default - or <em>DB_LOCK_EXCLUSIVE</em>); the ability to define the initial number of tables (default = 5, minimum = 1); and the initial amount of free space pointers to reserve space for (default = 5, minimum = 0). </p>

<p> The value returned by the routine gives as indication of a successful creation (<em>DB_OK</em> signifies); any other value indicates a problem - <em>DB_EXISTS_ALREADY</em> or an out-and-out failure. </p>

<p> Calling this function not only creates a new database but it also makes it the current database, even if another one was already active. </p>

<p> The routine <tt>db_select</tt> enables a user to switch between databases. This takes two parameters: the first two of <tt>db_create</tt>. Alternatively, you can call <tt>db_close</tt>(), which closes the current database and releases any lock on it. As you might expect, there is a routine <tt>db_open</tt>, again with the same two parameters as <tt>db_select</tt>, for making a pre-existing database the current one. If you ever need it, the function <tt>db_current</tt>() will always return the name of the active database. </p>

<p>  Over time your databases can get filled up with blank spaces, for example when you delete records from a table. You can optimise the current database by using <tt>db_compress</tt>(). Very rarely you might need to clear its cache, using <tt>db_cache_clear</tt>(). You can control whether or not caching takes place, using <tt>db_set_caching</tt>(TRUE|FALSE). The default case is for caching to be used. </p>

<a name="_3_creatingandhandlingtables"></a><a name="creatingandhandlingtables"></a><h3>Creating and handling tables</h3>
<p> A similar set of routines are available for creating and selecting tables within the active database. </p>

<p> You can create a new table using <tt>db_create_table</tt>, which takes two parameters: the text-string name and the number of record spaces to reserve initially (default = 50). As in the case of database creation, <em>DB_OK</em> is typically returned, unless <em>DB_EXISTS_ALREADY</em> applies. </p>

<p> You can determine which table is the active one, using <tt>db_current_table</tt>(), which returns its text-string name. You can use <tt>db_select_table</tt>(name) to make a particular table the active one, returning <em>DB_OK</em> normally, otherwise <em>DB_OPEN_FAIL</em>. </p>

<p> The routine <tt>db_table_list</tt>() returns a sequence containing the text-based names of all the tables in the current database. You can delete an existing table using <tt>db_delete_table</tt>(name), rename it using <tt>db_rename_table</tt>(oldname, newname). Also you can retain a table but, using <tt>db_clear_table</tt>(name), delete all its records. </p>

<p>  You can obtain information about a table. The function <tt>db_table_size</tt>([tablename]) gives the number of records in the table. </p>

<a name="_4_creatingandhandlingrecords"></a><a name="creatingandhandlingrecords"></a><h3>Creating and handling records</h3>
<p> For the active table in the current database, there are a range of routines to access and manipulate individual records. As a general rule, although searches can be made using keys many operations are faster using the index number, especially as you can loop through index numbers to get multiple values. </p>

<p> You can add a new entry to this table using <tt>db_insert</tt>(key, data). (It also works in a non-active table if you add the table name as the third parameter.) You can delete an existing record using <tt>db_delete_record</tt>(index), again adding the table name if it is inactive. You can retain the record but change the data value, using <tt>db_replace_data</tt>(key, newdata[, tablename]). Perhaps counter-intuitively, a faster way to do this is to use <tt>db_get_recid</tt>(key[, tablename]) and enter the return value into <tt>db_replace_recid</tt>(index, newdata). </p>

<p>   You can seek a given key, using <tt>db_find_key</tt>(key[, tablename]), which returns either the index number of the record or some form of error code. (Again it can be used on a non-active table by adding a second parameter giving the name of the alternative table.) You can input a record number to the routine <tt>db_record_data</tt>(index) to return the corresponding data item. Alternatively, you can obtain the data item directly from the key using <tt>db_fetch_record</tt>(key[, tablename]), which also can be used indirectly by adding the name of the table as the second parameter. If you want to find the key for a given index number then the function <tt>db_record_key</tt>(index[, tablename]) will achieve this. You can obtain both the key and data values via the record index using <tt>db_record_recid</tt>(index). </p>

<a name="_5_extensionmaterial"></a><a name="extensionmaterial"></a><h3>Extension material</h3>
<p> If you search one of the archival sources you can find various tools for augmenting the basic EDS provision. For example, you can download a GUI which will display details of an EDS database and reveal the contents of its tables. </p>

<a name="_6_otheraccesstodatabasemanagement"></a><a name="otheraccesstodatabasemanagement"></a><h2>Other access to database management</h2>
<p> Also available in the archived material are examples of specific database applications, not all of which use EDS as the manager. It is possible, however, that one may fit your needs or can easily be adapted - in itself a good programming exercise. </p>

<p> Several of the entries have a recurring theme: SQL. This seems to have overtaken most of the alternatives in recent times. If you are familiar with SQL syntax and want to interface SQL with Euphoria, then probably the best choice is to read <a class="external" href="https://openeuphoria.org/wiki/view/EDBI.wc">the EDBI wiki entry</a> and see if that suits you. </p>
</body></html>