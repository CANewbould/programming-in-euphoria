<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <title></title>
 <link rel="stylesheet" media="screen, projection, print" type="text/css" href="style.css"/>
<!-- source="chapter14.cr" -->
</head>
<body>

<a name="_0_chapter14extendingthecoreusinglibrarymodules"></a><a name="chapter14extendingthecoreusinglibrarymodules"></a><h1>Chapter 14: Extending the core using library modules</h1>
<p>  In this chapter we explore key aspects of Euphoria which, although, embedded in the Core, have no utility when only the Core is being used. You could say that they have been placed within the Core purely for Euphoria's expansion potential. </p>

<a name="_1_arecaponmodularity"></a><a name="arecaponmodularity"></a><h2>A recap on modularity</h2>
<p> For present purposes, a module is defined as a distinct code block, with its own (internal) properties and functionality, and its own internal consistency. In Euphoria a module is rendered distinct by being placed in a separate file and labelled, using the file extension, according to its properties: to the standard Euphoria extension (<em>.e</em>) is added, first by an <em>x</em>, but only if it has an executable main routine, then by a <em>w</em> but only if it contains any form of graphical user interface (the w of a window!).  Although the modular concept can equally apply to both an application and a <em>library</em> module, in this chapter we shall be looking solely at the latter, which are designed to offer added functionality to the Core. This is achieved by the library module presenting an interface, which enables properties and routines to be accessed by other modules, but only in accordance with a set of rules that have been determined by the author of that library. </p>

<p> A key concept in our understanding of such interfaces is that of <em>scope</em>. </p>

<a name="_2_arecaponscope"></a><a name="arecaponscope"></a><h2>A recap on scope</h2>
<p> If you are not sure about all of this, please take the time to revisit Chapter 10 before reading on, or read the following recaps. </p>

<a name="_3_export"></a><a name="export"></a><h3>export</h3>
<p> Placing the keyword <em>export</em> in front of any entity in a Euphoria module makes that entity visible in any other module which calls the source module. (How to call the module is considered later.) This visibility, however, cannot be transferred to a third module; every module that seeks access to that entity must explicity claim that access. </p>

<a name="_4_public"></a><a name="public"></a><h3>public</h3>
<p> Placing the keyword <em>public</em> in front of an entity in a module makes that entity not only visible in any calling module but also affords the potential for passing on that visibility to other modules, provided they access the intermediate module in an appropriate way (again see below for the mechanism). </p>

<a name="_5_usingthemoduleinanothermodule"></a><a name="usingthemoduleinanothermodule"></a><h2>Using the module in another module</h2>
<p> To link a module to another module involves opening up a one-way relationship to make accessible the non-local entities of another module. </p>

<a name="_6_include"></a><a name="include"></a><h3>include</h3>
<p> This is effected through use of the keyword <em>include</em>. </p>

<p> You code <em>include</em> followed by the name of the module (file name and extension) you wish to access. The file name my be prefaced by either an absolute folder reference, or by a relative one, but there are alternative ways to control the search paths of modules (see below). </p>

<p> If you wish to access more than one external module then you need a separate <em>include</em> for each request for access. </p>

<p> Provided the search path allows the Interpreter to find the included module, you will then have access to both <em>export</em>ed and <em>public</em> entities automatically. (You will get an error message at the first Interpreter pass if it cannot find the module!) </p>

<p> If you wish to "chain" modules then you can ensure that those entities declared as <em>public</em> are passed on up the chain by linking the two terms: </p>

<pre class="examplecode"><font color="#0000FF">public include </font><font color="#330033">module_name.extension</font>
</pre>

<a name="_7_usingeucfgtolocatelibraryfiles"></a><a name="usingeucfgtolocatelibraryfiles"></a><h3>Using eu.cfg to locate library files</h3>
<p> Although Euphoria was originally designed to work with the operating system's environmental variable provision, Open Euphoria's preferred approach is either to use command-line switches or (and this is advantageous once you have established double-click status on executables) to use a configuration file. </p>

<p> This file must be called <em>eu.cfg</em> and, although multiple instances and placements are possible, I want to stay with the simplest arrangement: place the file in the folder where you are making the top-level call to your modules. </p>

<p> Essentially the configuration file contains the command-line switch settings but in a slightly more convenient form. Any lines in the file not prefixed by the switch code itself will be interpreted as a search path prefix. So it is possible to include, for more complex set-ups, several search paths in one <em>eu.cfg</em> file. </p>

<a name="_8_examplesoflibraries"></a><a name="examplesoflibraries"></a><h2>Examples of libraries</h2>
<p> The thinking behind the use of modular library extensions is to keep the Core as small as possible, then to build extension modules based on a single philosophy. Below are listed, with a degree of detail, a description of that philosophical approach, followed by a general description of the result of applying that approach. Each section ends with a link to a chapter giving a much more detailed description of that result. </p>

<a name="_9_librariesassets"></a><a name="librariesassets"></a><h3>Libraries as sets</h3>
<p>  The early forms of Euphoria included the Core but packaged with a small number (13) of extension modules. In general the entities offered within (using the then only scope mechanism - <em>global</em>) were grouped into these modules on a theme-based principle, with just one miscellaneous (<em>misc.e</em>) module. </p>

<a name="_10_standardlibraries"></a><a name="standardlibraries"></a><h4>Standard libraries</h4>
<p> When Open Euphoria was introduced these libraries were still offered within the distribution but had been superceded by a host of "standard" libraries (now totally more than 50). The philosophical approach, however, remained the same: the criterion for grouping is thematic, with the emphasis on the functionality of the routines placed therein. To read more about the details of this approach go to <a href="chapter18.html">Chapter 18</a>. </p>

<a name="_11_objectcentredeuphoria"></a><a name="objectcentredeuphoria"></a><h4>Object-Centred Euphoria</h4>
<p> Alternative criteria are, of course possible. It is my view, repeated elsewhere in this e-book, that the grouping of entities by theme results from prioritising one aspect of Euphoria (it is <em>procedural</em>) over another (more important?) aspect - the nature of its data. </p>

<p> To illustrate this I have devised Object-Centred Euphoria, which extends the Core purely in relation to object <em>type</em>s. The approach adds just three libraries: </p>
<ul><li><em>atom.e</em>
</li><li><em>sequence.e</em>
</li><li><em>object.e</em>
</li></ul>
<p> the latter holding those generalised entities which can equally apply to both single- and multi-valued data. </p>

<p> The principles adopted in providing this alternative extension are simple: </p>
<ol><li>every routine must have at least one argument; that argument must refer to an object of a specific type
</li><li>routine names should be such that when read they sound like natural language
</li><li>full use should be made of the structures in the Core but little (ideally none) of the Core routines - these should be re-written to conform to OCE philosophy
</li><li>full use should be made of Euphoria's ability to assign default values to arguments in order to minimise the range of OCE routines
</li></ol>
<p>  In short, OCE seeks to offer an approach which places the emphasis on data structures; sees routines as servants of these types; appeals to natural language; has a minimum number of modules and a relatively small number of routines in each. </p>

<p>  To read about how this approach works out go to <a href="chapter22.html">Chapter 22</a>. </p>

<a name="_12_librariesinotherextensionstrategies"></a><a name="librariesinotherextensionstrategies"></a><h4>Libraries in other extension strategies</h4>
<p> Both these approaches utilise just one of the several extension options. In cases where more than one approach is being combined, and library modules are part of the overall strategy, it is invariably the case that the nature of library organisation is largely determined by the other extension approaches. </p>

<a name="_13_individuallibraryofferingdistinctformsofextension"></a><a name="individuallibraryofferingdistinctformsofextension"></a><h3>Individual library offering distinct forms of extension</h3>
<p> In this section we consider not an overall library extension strategy but something much simpler: the use of a single extension library to open up a specific field of processing opportunity. </p>

<p> Two examples are given and briefly described; each can be explored in detail in dedicated chapters later in this e-book. </p>

<p> As indicated already, a powerful extension option available to Euphoria is the ability to access routines, written in the C language and stored as a <em>dll</em> or <em>so</em> library. </p>

<a name="_14_theextensionlibrarydlle"></a><a name="theextensionlibrarydlle"></a><h4>The extension library dll.e</h4>
<p> As revealed earlier, there are things packaged in the core that are unused there but offer, through the machine-code interface, an extension opportunity. Probably the most powerful is access to C-language libraries. </p>

<p> The tools to achieve this have been organised into the library, <em>dll.e</em>, with versions in both the original RDS distribution and in the Open Euphoria standard library package. </p>

<p> The library offers a routine to open such a file; it is structured so that it works whatever the operating system and you can even write one call that will work on multiple OSs. A successfully opened shared library yields a handle which can be used as a direct link. </p>

<p> C-language functions can be linked, again through handles, directly to procedures (void functions in C) or to functions. Once defined these are called (using either <tt>c_proc</tt> or <tt>c_func</tt>, as appropriate) within the Euphoria code, as if the C-language function itself was being called directly. This works because the defining process "maps" C-language data types to Euphoria ones, so that the latter can be passed to the call, as proxies. </p>

<p> For the full story go to <a href="chapter19.html">Chapter 19</a>. </p>

<a name="_15_theeuphoriadatabasesystemeds"></a><a name="theeuphoriadatabasesystemeds"></a><h4>The Euphoria Database System (EDS)</h4>
<p>  Another module library, offered both in the original distributions and in the recent ones, within the standard library set, which offers a particular functionality is the library <em>eds.e</em>. Within this library are a set of routines to create, maintain and operate a fully-functional database management system. </p>

<p> More detail on these features is given in <a href="chapter20.html">Chapter 20</a>. </p>

<p> EDS is a pure Euphoria-based library and so it has no link with any other database management product. Many people are used to SQL and may prefer to utilise links between Euphoria and SQL via such devices as MySQL. A Euphoria wrapper for MySQL has been written; if you search the online Forum for "mySQL" you will find a suitable link. </p>

<a name="_16_buildingyourownmoduleset"></a><a name="buildingyourownmoduleset"></a><h2>Building your own module set</h2>
<p> Whilst not claming to be a manual for library-module development, a few aspects of guidance are offered here. </p>

<p>  Whilst I have argued elsewhere that generalising routines is a good practice to adopt, this can cause slowing down of Interpreter speed if you organise your library modules in such a way then, at interpreter-time, Euphoria is accessing lots of modules with only the odd fixed value or small routine, from a range of much bigger units. </p>

<p> So the general advice for all programming is to make modules as small as possible, even if that means there are quite a lot of them in your scheme. Coding blocks should rarely exceed 50 lines of code. (Note that the small-is-best strategy increases the need to be vigilant over scope and especially the use of <em>public</em> both as scope for elements but also for chaining of modules, through <em>public include</em>.) </p>

<p> If your module set uses lots of fixed values (<em>constant</em>s) then it may prove best to locate them in a single file, chained throughout. </p>

<p> Before embarking on your strategy, decide what your naming conventions are. If you favour short and simple names then the use of <em>namespace</em>s will be particularly useful. </p>

<a name="_17_namespaces"></a><a name="namespaces"></a><h3>Namespaces</h3>
<p>  If you do opt for short and simple names you will inevitably find that you want to use the same name(s) in different modules. Without qualification, the Interpreter will get confused (and tell you so) if it cannot distinguish, say, one <tt>open</tt> routine from another. This is true even if the two or more clashes differ in number of type of arguments. Euphoria, however, offers you two ways out of this dilema: </p>
<ol><li>add to a given module a unique name to associate with the module, via the <em>namespace</em> keyword, followed by that unique name
</li><li>when including a module in a calling module, append the keyword <em>as</em> to the <em>include</em> line, followed by the module name desired
</li></ol>
<p> The second method allows greater flexibility as the name is allocated dynamically; consequently on a different occasion a different name can be assigned. </p>

<p> An example follows. Suppose you have a module for file handling, stored in <em>file.e</em>, with a function called <tt>open</tt> declared as <em>public</em>; also a module for shared library handling, named <em>dll.e</em>, also containing a <em>public</em> <tt>open</tt> declaration. </p>

<p> In the first strategy: </p>

<pre class="examplecode"><font color="#FF0055">-- file.e</font>
<font color="#0000FF">namespace </font><font color="#330033">file </font>
<font color="#0000FF">public function open</font><font color="#880033">(</font><font color="#330033">..</font><font color="#880033">)</font>
</pre>

<p> and </p>

<pre class="examplecode"><font color="#FF0055">-- dll.e</font>
<font color="#0000FF">namespace </font><font color="#330033">dll </font>
<font color="#0000FF">public function open</font><font color="#880033">(</font><font color="#330033">..</font><font color="#880033">)</font>
</pre>

<p> and when calling: </p>

<pre class="examplecode"><font color="#0000FF">include </font><font color="#330033">file.e</font>
<font color="#330033">file:</font><font color="#0000FF">open</font><font color="#880033">(</font><font color="#330033">..</font><font color="#880033">) </font>
<font color="#FF0055">-- or </font>
<font color="#0000FF">include </font><font color="#330033">dll.e </font>
<font color="#330033">dll:</font><font color="#0000FF">open</font><font color="#880033">(</font><font color="#330033">..</font><font color="#880033">) </font>
<font color="#FF0055">-- as appropriate</font>
</pre>

<p> To implement the second approach, add to the include statement(s) as follows: </p>

<pre class="examplecode"><font color="#0000FF">include </font><font color="#330033">file.e </font><font color="#0000FF">as </font><font color="#330033">file</font>
<font color="#330033">file:</font><font color="#0000FF">open</font><font color="#880033">(</font><font color="#330033">..</font><font color="#880033">) </font>
<font color="#FF0055">-- or </font>
<font color="#0000FF">include </font><font color="#330033">dll.e </font><font color="#0000FF">as </font><font color="#330033">dll </font>
<font color="#330033">dll:</font><font color="#0000FF">open</font><font color="#880033">(</font><font color="#330033">..</font><font color="#880033">) </font>
<font color="#FF0055">-- as appropriate</font>
</pre>

<p> Although I would not recommend mixing the two styles, this second approach works even if there is a namespace declaration in a module being called. This is because the dynamic call takes precedence over the built-in one(s). </p>
</body></html>